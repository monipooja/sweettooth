//
//  RegisterViewController.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 3/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import SystemConfiguration

class RegisterViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var contactNumber: UITextField!
    @IBOutlet var emailId: UITextField!
    @IBOutlet var addressText: UITextField!
    @IBOutlet var passwordText: UITextField!
    @IBOutlet var confirmPassword: UITextField!
    @IBOutlet var pincodeText: UITextField!
    @IBOutlet var cityText: UITextField!
    @IBOutlet var countryText: UITextField!
    
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var confirmButton: UIButton!
    
    var check : Bool = false
    var imgdata : String = ""
    var newFrame : CGRect!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let textfield : [UITextField] = [firstName, lastName, contactNumber, emailId, addressText, passwordText, confirmPassword, pincodeText, cityText, countryText]
        
        let placeholder = ["First Name", "Last Name", "Mobile no", "Email Id", "Address", "Password", "Confirm Password", "Postcode", "City/Town", "County"]
        var i = 0
        
        for tf in textfield
        {
            tf.delegate = self
            let paddingForFirst = UIView(frame:  CGRect(x: 0, y: 0, width: 5, height: tf.frame.size.height))
            //Adding the padding to the second textField
            tf.leftView = paddingForFirst
            tf.leftViewMode = UITextFieldViewMode .always
            
            tf.attributedPlaceholder = NSAttributedString(string:placeholder[i],
                                                          attributes:[NSForegroundColorAttributeName: UIColor.white])
            i += 1
        }
        confirmButton.layer.cornerRadius = 20
    }

    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if (textField.placeholder == "County") || (textField.placeholder == "City/Town") || (textField.placeholder == "Postcode") || (textField.placeholder == "Email Id") || (textField.placeholder == "Password") || (textField.placeholder == "Confirm Password")
        {
            self.newFrame = self.view.frame
            
            newFrame.origin.y = self.view.bounds.origin.y - 110
            newFrame.size.height = self.view.bounds.size.height
            
            UIView.animate(withDuration: 0.25, animations:
                {() -> Void in
                    
                    self.view.frame = self.newFrame
            })
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if (textField.placeholder == "County") || (textField.placeholder == "City/Town") || (textField.placeholder == "Postcode") || (textField.placeholder == "Email Id") || (textField.placeholder == "Password") || (textField.placeholder == "Confirm Password")
        {
            newFrame.origin.y = 0
            newFrame.size.height = self.view.bounds.size.height
            
            UIView.animate(withDuration: 0.25, animations:
                {() -> Void in
                    self.view.frame = self.newFrame
            })
        }
    }
    
    @IBAction func cameraGallery(_ sender: Any) {
        
//        let imagePicker = UIImagePickerController()
//        imagePicker.delegate = self
//        ApiResponse.CameraGallery(controller: self, imagePicker: imagePicker)
    }

    @IBAction func onRegister(_ sender: Any) {
        
        if (firstName.text == "") || (lastName.text == "") || (contactNumber.text == "") || (emailId.text == "") || (addressText.text == "") || (passwordText.text == "") || (confirmPassword.text == ""){
            ApiResponse.alert(title: "Blank!", message: "Fields cannot be blanks", controller: self)
        }
        else if (pincodeText.text == "") {
            ApiResponse.alert(title: "Blank", message: "Postcode cannot be left blank", controller: self)
        }
        else if (cityText.text == "")  {
            ApiResponse.alert(title: "Blank", message: "City/Town cannot be left blank", controller: self)
        }
        else if (ApiResponse.validateEmail(emailId.text!) == false) {
            ApiResponse.alert(title: "Invalid Email", message: "Please enter valid email id", controller: self)
        }
        else if ((contactNumber.text?.characters.count)! < 11) || ((contactNumber.text?.characters.count)! > 11) {
            ApiResponse.alert(title: "Ooops", message: "Mobile No. is not recognised as a UK number", controller: self)
        }
        else if ((passwordText.text?.characters.count)! < 6) {
            ApiResponse.alert(title: "Ooops", message: "Password should be minimum 6 characters", controller: self)
        }
        else if (passwordText.text! != confirmPassword.text!) {
            ApiResponse.alert(title: "Password Mismatch", message: "Password confirmation does not match", controller: self)
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN  {
            
            firstName.text = firstName.text?.uppercaseFirst
            lastName.text = lastName.text?.uppercaseFirst
            emailId.text = emailId.text?.lowercased()
            cityText.text = cityText.text?.uppercaseFirst
            countryText.text = countryText.text?.uppercaseFirst
            addressText.text = addressText.text?.uppercaseFirst
            pincodeText.text = pincodeText.text?.uppercased()
            
            // let dict = ["fname" : firstName.text!, "lname" : lastName.text!, "contno" : contactNumber.text!, "email" : emailId.text!, "add" : addressText.text!, "pwd" : passwordText.text!, "imgdata" : imgdata]
            
            LoadingIndicatorView.show("Loading...")
            
            let params = "first_name=\(firstName.text!)&last_name=\(lastName.text!)&email=\(emailId.text!)&contact_no=\(contactNumber.text!)&password=\(passwordText.text!)&address=\(addressText.text!)&image=\(imgdata)&pincode=\(pincodeText.text!)&city=\(cityText.text!)&state=NA&country=\(countryText.text!)&type=ios"
            
            ApiResponse.onResponsePostPhp(url: "home/attemptsignup", parms: params, completion: { (result, error) in
                
                if (error == "") {
                    
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    
                    if (status == true) {
                        
                        OperationQueue.main.addOperation {
                            LoadingIndicatorView.hide()
                            user.set(true, forKey: "logout")
                            let datadict = result["data"] as! NSDictionary
                            user.set(datadict, forKey: "logindata")
                            user.synchronize()
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.add111()
                        }
                    }
                    else {
                        ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
            ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
}
