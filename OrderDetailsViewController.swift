//
//  OrderDetailsViewController.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 4/19/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import SystemConfiguration

class OrderDetailsViewController: UIViewController {
    
    @IBOutlet var orderDate: UILabel!
    @IBOutlet var orderId: UILabel!
    @IBOutlet var addressView: UIView!
    @IBOutlet var customerName: UILabel!
    @IBOutlet var customerAddress: UILabel!
    @IBOutlet var orderTable: UITableView!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var restAddress: UILabel!
    
    @IBOutlet var restImage: UIImageView!
    @IBOutlet var restName: UILabel!
    @IBOutlet var restContact: UILabel!
    
    @IBOutlet var addConstraints: NSLayoutConstraint!
    @IBOutlet var contentHtConstraint: NSLayoutConstraint!
    @IBOutlet var tableHtConstraint: NSLayoutConstraint!
    @IBOutlet var addressvwHtConts: NSLayoutConstraint!
    @IBOutlet var addressTextHtconst: NSLayoutConstraint!
    
    var productArray : [NSDictionary] = []
    var totPrc : Float = 0.00
    var orderId1 : String = ""
    var totalQty : Float = 0.00
    var subtotalQty : Float = 0.00
    var deliveryFee : Float = 0.00
    var theJSONText : NSString!
    var ht : Int!
    
    var ordeDateShow : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addressView.layer.cornerRadius = 5.0
        addressView.layer.borderColor = UIColor.lightGray.cgColor
        addressView.layer.borderWidth = 2.0
    }
    
    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: { () -> Void in
            LoadingIndicatorView.show()
            self.getOrderList()
        })
    }
    
    func getOrderList()
    {
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let params = "order_id=\(orderId1)"
            ApiResponse.onResponsePostPhp(url: "home/userodeardetails", parms: params, completion: { (result, error) in
                
                if (error == "") {
                    
                    print("history result = \(result)")
                    
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    if (status == true)
                    {
                        OperationQueue.main.addOperation {
                            let temp = result["data"] as! NSDictionary
                            self.productArray = temp["odeardetails"] as! [NSDictionary]
                            let dict12 = self.productArray[0] as NSDictionary
                            
                            let  userDetailArray = temp["userdetails"] as! NSDictionary
                            
                            self.customerName.text = "\(userDetailArray["first_name"] as! String) \(userDetailArray["last_name"] as! String)"
                            self.restImage.sd_setImage(with: NSURL(string : "\(Constant.BASE_URL)/\(dict12["restaurant_image"] as! String)") as URL!)
                            self.restName.text = (dict12["restaurant_name"] as! String)
                            self.restAddress.text = "\(dict12["restaurant_address"] as! String), \(dict12["restaurant_city"] as! String), \((dict12["restaurant_pincode"] as! String).uppercased())"
                            let adht = ApiResponse.calculateHeightForString("\(dict12["restaurant_address"] as! String), \(dict12["restaurant_city"] as! String), \((dict12["restaurant_pincode"] as! String).uppercased())")
                            self.addConstraints.constant = 10 + adht
                            self.restAddress.numberOfLines = 0
                            
                            self.restContact.text = "Contact number - \(dict12["restaurant_contact_number"] as! String)"
                            
                            self.orderId.text = "Order No. - \(self.orderId1)"
                            self.orderDate.text = "\(dict12["delivery_date"] as! String) (\(dict12["delivery_time"] as! String))"
                            
                            let httt = ApiResponse.calculateHeightForString("\(userDetailArray["address"] as! String), \(userDetailArray["city"] as! String), \(userDetailArray["pincode"] as! String)")
                            
                            let dlvryType = userDetailArray["product_delivery_type"] as? String
                            
                            if (dlvryType == "Delivery") || (dlvryType == "delivery") {
                                self.addressTextHtconst.constant = httt + 10
                                self.addressvwHtConts.constant = 120 + httt
                                
                                self.customerAddress.text = "\(userDetailArray["address"] as! String), \(userDetailArray["city"] as! String), \((userDetailArray["pincode"] as! String).uppercased())"
                                self.customerAddress.numberOfLines = 0
                            }
                            else {
                                self.addressTextHtconst.constant = 0
                                self.addressvwHtConts.constant = 100
                            }
                            if (self.productArray.count != 0)
                            {
                                self.orderTable.reloadData()
                                self.setScrollView()
                            }
                            else {
                                self.orderTable.isHidden = true
                            }
                            LoadingIndicatorView.hide()
                        }
                    }
                    else {
                        ApiResponse.alert(title: "Ooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
            ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }
    
    func setScrollView()
    {
        if (productArray.count  != 0)
        {
            if (productArray.count == 1) {
                ht = ((productArray.count) * 45)
                tableHtConstraint.constant = orderTable.bounds.size.height
            }
            else {
                ht = ((productArray.count - 1) * 45)
                tableHtConstraint.constant = (orderTable.bounds.size.height)+CGFloat(ht)
            }
            
            scrollView.isScrollEnabled = true
            contentHtConstraint.constant = 320 + tableHtConstraint.constant + 25 + addressvwHtConts.constant
        }
        else
        {
            orderTable.isHidden = true
            
            let lab =  UILabel.init()
            lab.frame.size = CGSize(width : 200, height : 50)
            lab.center = self.view.center
            lab.text = "No Order"
            lab.textAlignment = NSTextAlignment.center
            lab.font = UIFont(name: "Open Sans", size: 18)
            self.view.addSubview(lab)
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
}

class orderDetailCell: UITableViewCell {
    
    @IBOutlet var dishName: UILabel!
    @IBOutlet var dishQuantity: UILabel!
    @IBOutlet var dishPrice: UILabel!
}

class subtotalTableViewCell: UITableViewCell {
    
    @IBOutlet var subtotal: UILabel!
    @IBOutlet var deliveryFee: UILabel!
    
}

class totalOrderCell : UITableViewCell
{
    @IBOutlet var totalAmount: UILabel!
}

extension OrderDetailsViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 1)
        {
            return productArray.count
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section == 0)
        {
            let cell = orderTable.dequeueReusableCell(withIdentifier: "cell")
            
            return cell!
        }
        else if (indexPath.section == 1)
        {
            let cell = orderTable.dequeueReusableCell(withIdentifier: "detail")! as! orderDetailCell
            
            let dict = productArray[indexPath.row]
            cell.dishName.text = dict["product_name"] as? String
            let twoDecimalPlaces1 = String(format: "%.2f", Float((dict["price"] as! String))!)
            cell.dishPrice.text = "\u{00A3}\(twoDecimalPlaces1)"
            cell.dishQuantity.text = "(\(dict["quantity"] as! String))"
      
            subtotalQty = subtotalQty + ((dict["price"] as! NSString).floatValue)
            deliveryFee =  Float((dict["delivery_fee"] as! String))!
            
            return cell
        }
        else if (indexPath.section == 2)
        {
            let cell = orderTable.dequeueReusableCell(withIdentifier: "subtotalcell")! as! subtotalTableViewCell
            
            let twoDecimalPlaces2 = String(format: "%.2f", Float(subtotalQty))
            cell.subtotal.text = "\u{00A3}\(twoDecimalPlaces2)"
            let twoDecimalPlaces3 = String(format: "%.2f", deliveryFee)
            cell.deliveryFee.text = "\u{00A3}\(twoDecimalPlaces3)"
            
            totalQty = totalQty + Float(twoDecimalPlaces2)! + Float(twoDecimalPlaces3)!
            
            return cell
        }
        else
        {
            let cell = orderTable.dequeueReusableCell(withIdentifier: "total")! as! totalOrderCell
            
            let twoDecimalPlaces2 = String(format: "%.2f", Float(totalQty))
            cell.totalAmount.text = "\u{00A3}\(twoDecimalPlaces2)"
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (indexPath.section == 2) {
            return 60
        }
        else  {
            return 45
        }
    }
}
