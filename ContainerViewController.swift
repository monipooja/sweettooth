

import UIKit
import SlideMenuControllerSwift

class ContainerViewController: SlideMenuController
 {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    static func loadMainControllerWithHamburgerMenu() -> SlideMenuController
    {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainController = mainStoryboard.instantiateViewController(withIdentifier: DashboardViewController.identifier!)
        let hamburgerController = mainStoryboard.instantiateViewController(withIdentifier: SideTableViewController.identifier!)
        return SlideMenuController(mainViewController: mainController, leftMenuViewController: hamburgerController)
    }

}
