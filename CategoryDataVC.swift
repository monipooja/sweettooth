//
//  CategoryDataVC.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 4/3/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import SystemConfiguration

class CategoryDataVC: UIViewController {
    
    @IBOutlet var dishTableView: UITableView!
    @IBOutlet var categoryTableView: UITableView!
   
    @IBOutlet var categoryButton: UIButton!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var cartItems: UILabel!
    @IBOutlet var dessertTableHt: NSLayoutConstraint!
    
    var dataArray : [NSDictionary] = []
    var catgArray : [NSDictionary] = []
    var catgNameArray : [String] = []
    var catgIdArray : [String] = []
    
    var restaurentId : String = ""
    var restName : String = ""
    var stop : String = ""
    var userid : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        categoryTableView.isHidden = true
        
        navBar.topItem?.title = restName
        
        userid = (user.object(forKey: "logindata") as! NSDictionary)["user_id"] as! String
        
        catgNameArray = ["All Dessert"]
        catgIdArray = ["0"]
        categoryButton.setTitle(catgNameArray[0], for: .normal)
        categoryButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 5)
        
        DispatchQueue.main.async(execute: { () -> Void in
        
            LoadingIndicatorView.show()
            let params = "restaurant_id=\(self.restaurentId)"
            self.GetCategoryData("restaurant/categories", Parameters: params, check: "get")
        })
    }
    
    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
         cartItems.text = (user.object(forKey: "itemcount") as! String)
    }
    
    func GetCategoryData(_ urlStr : String, Parameters : String, check : String)
    {
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN {
            
            ApiResponse.onResponsePostPhp(url: urlStr, parms: Parameters, completion: { (result, error) in
                
                if (error == "") {
                   // print("categoreis data result = \(result) \(check)")
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    if (status == true) {
                        OperationQueue.main.addOperation {
                            
                            LoadingIndicatorView.hide()
                            if (check == "get") || (check == "catg") {
                                self.dataArray = result["data"] as! [NSDictionary]
                                self.catgArray = result["categories"] as! [NSDictionary]
                                for cat in self.catgArray {
                                    self.catgNameArray.append(cat["name"] as! String)
                                    self.catgIdArray.append(cat["id"] as! String)
                                }
                                self.dishTableView.reloadData()
                            }
//                            else if (check == "add") {
//                                let data = result["data"] as! NSDictionary
//                                print("ccccccc = \((data["count"] as? NSNumber)!)")
//                                self.cartItems.text = "\((data["count"] as? NSNumber)!)"
//                                user.set(self.cartItems.text!, forKey: "itemcount")
//                                user.synchronize()
//                                ApiResponse.alert(title: "Done", message: message, controller: self)
//                            }
                            else if (check == "load"){
                                print("dtata load = \(result)")
                                let dd = result["data"] as! [NSDictionary]
                                if (dd.count != 0){
                                    self.dataArray = self.dataArray + dd
                                }
                                else {
                                    self.stop = "stop"
                                }
                                self.dishTableView.reloadData()
                            }
                            else {
                                let data = result["data"] as! NSDictionary
                                self.cartItems.text = "\((data["count"] as? NSNumber)!)"
                                user.set(self.cartItems.text!, forKey: "itemcount")
                                user.synchronize()
                                ApiResponse.alert(title: "Done", message: message, controller: self)
                            }
                        }
                    }
                    else {
                        OperationQueue.main.addOperation {
                            if (check == "load") {
                                self.stop = "stop"
                                ApiResponse.alert(title: "Ooops", message: message, controller: self)
                            }
                            else if (check == "add") {
                                LoadingIndicatorView.hide()
                                let alert = UIAlertController(title: "Start a new order?", message: message, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: { action in
                                    let params = "user_id=\(self.userid)"
                                    self.GetCategoryData("cart/removecartitem", Parameters: params, check: "remove")
                                    
                                }))
                                alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            else {
                                ApiResponse.alert(title: "Ooops", message: message, controller: self)
                            }
                        }
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
            ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }
    
    @IBAction func openCategoryList(_ sender: Any) {
        categoryTableView.isHidden = false
        let htt = 44 * catgNameArray.count
        if (htt < 350) {
            dessertTableHt.constant = CGFloat(htt)
        }
        else {
            dessertTableHt.constant = 350
        }
        categoryTableView.reloadData()
    }
    
    @IBAction func openCart(_ sender: Any) {
        if (self.cartItems.text == "") || (self.cartItems.text == "0") {
            ApiResponse.alert(title: "Oooops", message: "Cart is empty", controller: self)
        }
        else {
            let cart = self.storyboard?.instantiateViewController(withIdentifier: "cart") as! MyCartViewController
            self.present(cart, animated: true, completion: nil)
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
}

class DishTableViewCell : UITableViewCell {
    
    @IBOutlet var dishImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var descLabel: UILabel!
}

extension CategoryDataVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (tableView == dishTableView) {
            return dataArray.count
        }
        else {
            return catgNameArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (tableView == dishTableView) {
        
            let cell : DishTableViewCell = dishTableView.dequeueReusableCell(withIdentifier: "dishcell") as! DishTableViewCell
            
            let dict = dataArray[indexPath.row]
            cell.nameLabel.text = dict["sweets_name"] as? String
            cell.descLabel.text = dict["dessert_description"] as? String
            let twoDecimalPlaces = String(format: "%.2f", Float((dict["price"] as? String)!)!)
            cell.priceLabel.text = "\u{00A3}\(twoDecimalPlaces)"
            cell.dishImage.sd_setImage(with: NSURL(string : "\(Constant.BASE_URL)\(dict["sweets_image"] as! String)") as URL!)
            
            return cell
        }
        else {
            let cell = UITableViewCell(style : .default, reuseIdentifier : "catcell")
            
            cell.textLabel?.text = catgNameArray[indexPath.row]
            cell.textLabel?.font = UIFont(name: "Open Sans", size: 15)
            cell.textLabel?.textColor = UIColor.white
            cell.backgroundColor = UIColor.init(red: 165/255.0, green: 86/255.0, blue: 51/255.0, alpha: 1)

            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (tableView == dishTableView) {
            let add = self.storyboard?.instantiateViewController(withIdentifier: "addcart") as! AddToCartVC
            add.passDict = self.dataArray[indexPath.row]
            self.present(add, animated: true, completion: nil)
        }
        else {
            let catid = catgIdArray[indexPath.row]
            categoryButton.setTitle(catgNameArray[indexPath.row], for: .normal)
            catgNameArray = ["All Dessert"]
            catgIdArray = ["0"]
            DispatchQueue.main.async(execute: { () -> Void in
                
                LoadingIndicatorView.show()
                let params = "restaurant_id=\(self.restaurentId)&cat_id=\(catid)"
                self.GetCategoryData("restaurant/categories", Parameters: params, check: "catg")
            })
            categoryTableView.isHidden = true
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if (tableView == dishTableView) {
            if indexPath.row == self.dataArray.count - 1
            {
                if (stop == "") {
                    LoadingIndicatorView.show()
                    let params = "restaurant_id=\(self.restaurentId)&page_no=\(dataArray.count)"
                    
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.GetCategoryData("restaurant/categories", Parameters: params, check: "load")
                    })
                }
            }
        }
    }
    
    
//    func AddToCart(_ sender : UIButton) {
//        
//        let point : CGPoint = sender.convert(CGPoint.zero, to: DataCollectionView)
//        let indexPath1 = DataCollectionView.indexPathForItem(at: point)
//        
//        let dict = dataArray[(indexPath1?.row)!]
//        
//        let userid = (user.object(forKey: "logindata") as! NSDictionary)["user_id"] as! String
//        LoadingIndicatorView.show()
//        let params = "user_id=\(userid)&product_id=\(dict["id"] as! String)&restaurant_id=\(dict["restaurant_id"] as! String)"
//        self.GetCategoryData("cart/add_cart", Parameters: params, check: "add")
//    }
    
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if indexPath.row == self.dataArray.count - 1
//        {
//            if (stop == "") {
//                LoadingIndicatorView.show()
//                let params = "restaurant_id=\(self.restaurentId)&page_no=\(dataArray.count)"
//                
//                DispatchQueue.main.async(execute: { () -> Void in
//                    self.GetCategoryData("restaurant/categories", Parameters: params, check: "load")
//                })
//            }
//        }
//    }
    
}
