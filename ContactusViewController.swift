//
//  ContactusViewController.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 3/30/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import SystemConfiguration

class ContactusViewController: UIViewController, UITextViewDelegate {

    @IBOutlet var messageTextview: UITextView!
    
    @IBOutlet var submitButton: UIButton!
    
    var userid : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        submitButton.layer.cornerRadius = 20
        
        messageTextview.text = "Send a message to Sweet Tooth...."
        messageTextview.textColor = UIColor.lightGray
        messageTextview.delegate = self
        
        messageTextview.layer.borderWidth = 1.0
        messageTextview.layer.borderColor = UIColor.white.cgColor
        messageTextview.layer.cornerRadius = 10.0
        
        userid = (user.object(forKey: "logindata") as! NSDictionary)["user_id"] as! String
    }

    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText : NSString = textView.text as NSString
        let updatedText = currentText.replacingCharacters(in: range, with: text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {
            
            textView.text = "Send a message to Sweet Tooth...."
            textView.textColor = UIColor.lightGray
            
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            
            return false
        }

        else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.white
        }
        
        if(text == "\n") {
            textView.resignFirstResponder()
//            textView.text = "Send a message to Sweet Tooth...."
//            textView.textColor = UIColor.lightGray
//            
//            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            return false
        }
        return true
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        
        if (messageTextview.textColor == UIColor.lightGray)
        {
            ApiResponse.alert(title: "", message: "Message cannot be blank", controller: self)
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN {
            
            LoadingIndicatorView.show()
            let params = "user_id=\(userid)&message=\(messageTextview.text!)"
            ApiResponse.onResponsePostPhp(url: "home/contact_us", parms: params, completion: {(result, error) in
            
                if (error == "")
                {
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    if (status == true) {
                        OperationQueue.main.addOperation {
                            ApiResponse.alert(title: "Done", message: message, controller: self)
                            self.viewDidLoad()
                        }
                    }
                    else {
                        ApiResponse.alert(title: "Oooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
            ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }

    @IBAction func openDashboard(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.add111()
    }
}
