//
//  OrderViewController.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 4/5/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import FSCalendar
import SystemConfiguration

class OrderViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, FSCalendarDataSource, FSCalendarDelegate {

    @IBOutlet var itmeLabel: UILabel!
    @IBOutlet var totalButton: UIButton!
    @IBOutlet var firstName: UITextField!
    @IBOutlet var contactnumber: UITextField!
    @IBOutlet var deliveryAddress: UITextView!
    @IBOutlet var cityText: UITextField!
    @IBOutlet var pincode: UITextField!
    @IBOutlet var noteText: UITextField!
    
    @IBOutlet var deliveryButton: UIButton!
    @IBOutlet var dateButton: UIButton!
    @IBOutlet var timeButton: UIButton!
    
    @IBOutlet var paymentButton: UIButton!
    
    @IBOutlet var timeTableVw: UITableView!
    
    @IBOutlet var calendarView: FSCalendar!
    @IBOutlet var addressHt: NSLayoutConstraint!
    @IBOutlet var cityHt: NSLayoutConstraint!
    @IBOutlet var pincodeHt: NSLayoutConstraint!
    
    @IBOutlet var addressLabelHt: NSLayoutConstraint!
    @IBOutlet var cityLabelHt: NSLayoutConstraint!
    @IBOutlet var pincodeLabelHt: NSLayoutConstraint!
    
    var itemNumber : Int = 0
    var totalPrc : String = ""
    var passArray : [NSDictionary] = []
    var nameString : String = ""
    var newFrame : CGRect!
    var passJsonText : NSString!
    var userid : String = ""
    var timeArray : [String] = []
    var tableName : String = ""
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let tfArray : [UITextField] = [firstName, contactnumber, pincode, noteText, cityText]
        let placeholder = ["Full Name", "Contact Number", "Postcode", "Leave a note for retailer", "City/Town"]
        var i = 0
        
        for tf in tfArray
        {
            tf.delegate = self
            let paddingForFirst = UIView(frame:  CGRect(x: 0, y: 0, width: 10, height: tf.frame.size.height))
            //Adding the padding to the second textField
            tf.leftView = paddingForFirst
            tf.leftViewMode = UITextFieldViewMode .always
            
            tf.attributedPlaceholder = NSAttributedString(string:placeholder[i],
                                                          attributes:[NSForegroundColorAttributeName: UIColor.white])
            i += 1
        }
        
        itmeLabel.text = "ITEMS (\(itemNumber))"
        let twoDecimalPlaces1 = String(format: "%.2f", Float(totalPrc)!)
        totalButton.setTitle("\u{00A3}\(twoDecimalPlaces1)", for: .normal)
        
        let datadict = user.object(forKey: "logindata") as! NSDictionary
        firstName.text = "\((datadict["first_name"] as? String)!) \((datadict["last_name"] as? String)!)"
        contactnumber.text = datadict["contact_no"] as? String
        pincode.text = (datadict["pincode"] as? String)?.uppercased()
        cityText.text = datadict["city"] as? String
        
        userid = datadict["user_id"] as! String
        
        deliveryAddress.text = datadict["address"] as? String
        deliveryAddress.delegate = self
        
        let btnArray : [UIButton] = [deliveryButton, dateButton, timeButton]
        for btn in btnArray {
            btn.titleEdgeInsets.left = 5.0
            btn.layer.borderWidth = 1.0
            btn.layer.borderColor = UIColor.white.cgColor
        }
        
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        doneToolbar.backgroundColor = UIColor.init(red: 0/255.0, green: 150/255.0, blue: 136/255.0, alpha: 1)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(OrderViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        contactnumber.inputAccessoryView = doneToolbar
        
        let date1 = NSDate()
        let dateFormatter11 = DateFormatter()
        dateFormatter11.dateFormat = "dd/MM/yyyy"
        dateButton.setTitle("\(dateFormatter11.string(from: date1 as Date))", for: .normal)
        
        let dateFormatter22 = DateFormatter()
        dateFormatter22.dateFormat = "hh:mm"
        timeButton.setTitle("\(dateFormatter22.string(from: date1 as Date))", for: .normal)
        
        //print("the json text = \(passJsonText))")
    }

    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func doneButtonAction()
    {
       contactnumber.resignFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        self.newFrame = self.view.frame
        
        newFrame.origin.y = self.view.bounds.origin.y - 110
        newFrame.size.height = self.view.bounds.size.height
        
        UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                
                self.view.frame = self.newFrame
        })
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        newFrame.origin.y = 0
        newFrame.size.height = self.view.bounds.size.height
        
        UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                self.view.frame = self.newFrame
        })
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.newFrame = self.view.frame
        
        newFrame.origin.y = self.view.bounds.origin.y - 110
        newFrame.size.height = self.view.bounds.size.height
        
        UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                
                self.view.frame = self.newFrame
        })
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        newFrame.origin.y = 0
        newFrame.size.height = self.view.bounds.size.height
        
        UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                self.view.frame = self.newFrame
        })
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText : NSString = textView.text as NSString
        let updatedText = currentText.replacingCharacters(in: range, with: text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {
            
            textView.text = "Delivery Address"
            textView.textColor = UIColor.lightGray
            
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            
            return false
        }
        else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.white
        }
        
        if(text == "\n") {
            textView.resignFirstResponder()
            textView.text = "Delivery Address"
            textView.textColor = UIColor.lightGray
            
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            return false
        }
        return true
    }
    
    func proceedToPayment(_ urlStr : String, params : String, check : String)
    {
        if (deliveryButton.titleLabel?.text == "Delivery") || (deliveryButton.titleLabel?.text == "delivery")
        {
            if (cityText.text == "") {
                ApiResponse.alert(title: "Empty", message: "Please enter city/town", controller: self)
            }
            else if (deliveryAddress.textColor == UIColor.lightGray) {
                ApiResponse.alert(title: "Oooops", message: "Please enter delivery address", controller: self)
            }
            else if (pincode.text == "") {
                ApiResponse.alert(title: "Oooops", message: "Please enter postcode", controller: self)
            }
        }
        if (firstName.text == "") || (contactnumber.text == "") {
            ApiResponse.alert(title: "Empty", message: "Please enter name and contact number", controller: self)
        }
        else if ((contactnumber.text?.characters.count)! < 11) || ((contactnumber.text?.characters.count)! > 11) {
            ApiResponse.alert(title: "Ooops", message: "Mobile No. is not recognised as a UK number", controller: self)
        }
        else if (dateButton.titleLabel?.text == "Delivery date") {
            ApiResponse.alert(title: "Oooops", message: "Please select \((deliveryButton.titleLabel?.text)!) date", controller: self)
        }
        else if (timeButton.titleLabel?.text == "Delivery time") {
            ApiResponse.alert(title: "Oooops", message: "Please select  \((deliveryButton.titleLabel?.text)!) time", controller: self)
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            firstName.text = firstName.text?.capitalized
            cityText.text = cityText.text?.uppercaseFirst
            noteText.text = noteText.text?.uppercaseFirst
            deliveryAddress.text = deliveryAddress.text.uppercaseFirst
            pincode.text = pincode.text?.uppercased()
            
            LoadingIndicatorView.show()
            ApiResponse.onResponsePostPhp(url: urlStr, parms: params, completion: { (result, error) in
                
                if (error == "") {
                    
                    print("Proceed Payment result = \(result)")
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    
                    if (status == true) {
                        
                        OperationQueue.main.addOperation {
                            LoadingIndicatorView.hide()
                            let dataArray = result["data"] as! [NSDictionary]
                            
                            let pay = self.storyboard?.instantiateViewController(withIdentifier: "payment") as! PaymentViewController
                            pay.priceStr = (self.totalButton.titleLabel?.text)!
                            pay.totalItemCount = self.itemNumber
                            pay.passArray = dataArray
                            self.present(pay, animated: true, completion: nil)
                        }
                    }
                    else {
                        ApiResponse.alert(title: "Ooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
            ApiResponse.alert(title: "No internet", message: "No internet connection", controller: self)
        }
    }

    @IBAction func proceedToPayment(_ sender: Any) {
        
        if (deliveryButton.titleLabel?.text == "Delivery") || (deliveryButton.titleLabel?.text == "delivery")
        {
            let parameters = "data=\(passJsonText!)&first_name=\(firstName.text!)&contact_no=\(contactnumber.text!)&pincode=\(pincode.text!)&address=\(deliveryAddress.text!)&paymenttype=COD&user_id=\(userid)&note=\(noteText.text!)&city=\(cityText.text!)&delivery_date=\((dateButton.titleLabel?.text)!)&delivery_time=\((timeButton.titleLabel?.text)!)&product_delivery_type=\((deliveryButton.titleLabel?.text)!)"
            
            print("papappaap = \(parameters)")
            self.proceedToPayment("home/addordersdetails", params: parameters, check: "pay")
        }
        else {
            if (deliveryAddress.textColor == UIColor.lightGray) {
                let parameters = "data=\(passJsonText!)&first_name=\(firstName.text!)&contact_no=\(contactnumber.text!)&pincode=\(pincode.text!)&address=&paymenttype=COD&user_id=\(userid)&note=\(noteText.text!)&city=\(cityText.text!)&delivery_date=\((dateButton.titleLabel?.text)!)&delivery_time=\((timeButton.titleLabel?.text)!)&product_delivery_type=\((deliveryButton.titleLabel?.text)!)"
                
                //print("papappaap = \(parameters)")
                self.proceedToPayment("home/addordersdetails", params: parameters, check: "pay")
            }
            else {
                let parameters = "data=\(passJsonText!)&first_name=\(firstName.text!)&contact_no=\(contactnumber.text!)&pincode=\(pincode.text!)&address=\(deliveryAddress.text!)&paymenttype=COD&user_id=\(userid)&note=\(noteText.text!)&city=\(cityText.text!)&delivery_date=\((dateButton.titleLabel?.text)!)&delivery_time=\((timeButton.titleLabel?.text)!)&product_delivery_type=\((deliveryButton.titleLabel?.text)!)"
                
               // print("papappaap = \(parameters)")
                self.proceedToPayment("home/addordersdetails", params: parameters, check: "pay")
            }
        }
    }
    
    @IBAction func onSelectTime(_ sender: Any) {
        tableName = "time"
        timeArray = []
        self.arrangeTime()
    }
    
    func arrangeTime()
    {
        let date = NSDate()
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "HH"
        
        let timestr = dateformatter.string(from: date as Date)

        let currentDate = Date()
        let dateString = self.dateFormatter.string(from: currentDate)
        print("today date = \(dateString)")
        
        if (dateButton.titleLabel?.text == dateString) {
            
            let remainingHr = 24 - Int(timestr)!
            
            for hh in 1...remainingHr-1 {
                
                timeArray.append("\(hh + Int(timestr)!):00")
                timeArray.append("\(hh + Int(timestr)!):30")
            }
        }
        else {
            for hh1 in 0...23 {
                timeArray.append("\(hh1):00")
                timeArray.append("\(hh1):30")
            }
        }
        timeTableVw.frame = CGRect(x : 32, y : 104, width : 130, height : 399)
        timeTableVw.reloadData()
        timeTableVw.isHidden = false
    }
    
    @IBAction func deliveryAction(_ sender: Any) {
        timeTableVw.frame = CGRect(x : 32, y : 455, width : 130, height : 116)
        tableName = "deliver"
        timeArray = []
        timeArray = ["Delivery", "Collection"]
        timeTableVw.reloadData()
        timeTableVw.isHidden = false
    }
    
    @IBAction func openCalendar(_ sender: Any) {
        calendarView.isHidden = false
//      timeButton.setTitle("Time", for: .normal)
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition)
    {
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "dd/MM/yyyy"
        let deliverDate = "\(dateFormatter2.string(from: date as Date))"
        
        dateButton.setTitle(deliverDate, for: .normal)
        timeButton.setTitle("Time", for: .normal)
        calendar.isHidden = true
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }
    
    public func minimumDate(for calendar: FSCalendar) -> Date
    {
        return Date() // crash!
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}

extension OrderViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : UITableViewCell =  timeTableVw.dequeueReusableCell(withIdentifier: "timecell")!
        
        cell.textLabel?.text = timeArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (tableName == "time") {
            timeButton.setTitle(timeArray[indexPath.row], for: .normal)
        }
        else {
            deliveryButton.setTitle(timeArray[indexPath.row], for: .normal)
            if (indexPath.row == 1) {
                addressHt.constant = 0
                cityHt.constant = 0
                pincodeHt.constant = 0
                addressLabelHt.constant = 0
                cityLabelHt.constant = 0
                pincodeLabelHt.constant = 0
            }
            else {
                addressHt.constant = 72
                cityHt.constant = 35
                pincodeHt.constant = 35
                addressLabelHt.constant = 2
                cityLabelHt.constant = 2
                pincodeLabelHt.constant = 2
            }
        }
          timeTableVw.isHidden = true
    }
}


