//
//  MyCartViewController.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 4/4/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import SystemConfiguration

var back : String = ""

class MyCartViewController: UIViewController {

    @IBOutlet var placeButton: UIButton!
    @IBOutlet var cartTableView: UITableView!
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var contentview: UIView!
    @IBOutlet var navBar: UINavigationBar!
    
    @IBOutlet var restaurentName: UILabel!
    @IBOutlet var restaurentAddress: UILabel!
    
    
    @IBOutlet var contentHtConstraint: NSLayoutConstraint!
    @IBOutlet var tableHtConstraint: NSLayoutConstraint!
    
    var productArray : [NSDictionary] = []
    var totPrc : Float = 0.00
    var userid : String = ""
    var totalQty : Int = 0
    var theJSONText : NSString!
    var ht : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        userid = (user.object(forKey: "logindata") as! NSDictionary)["user_id"] as! String
        
        DispatchQueue.main.async(execute: { () -> Void in
            
            LoadingIndicatorView.show()
            let parameters = "user_id=\(self.userid)"
            self.GetCartList("home/show_cartdetails", params: parameters, check: "list")
        })
    }
 
    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (back != "") {
            DispatchQueue.main.async(execute: { () -> Void in
                self.tableHtConstraint.constant = self.cartTableView.bounds.size.height
                self.productArray = []
                self.totPrc = 0.00
                self.totalQty = 0
                let parameters = "user_id=\(self.userid)"
                self.GetCartList("home/show_cartdetails", params: parameters, check: "list")
            })
        }
    }

    func GetCartList(_ urlstr : String, params : String, check : String)
    {
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN {
            
            ApiResponse.onResponsePostPhp(url: urlstr, parms: params, completion: { (result, error) in
                
                if (error == "") {
                    print("cart result = \(result)")
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    if (status == true) {
                        OperationQueue.main.addOperation {
                            
                            LoadingIndicatorView.hide()
                            if (check == "list") {
                                self.productArray = result["data"] as! [NSDictionary]
                                
                                for prod in self.productArray {
                                    
                                    self.restaurentName.text = prod["restaurant_name"] as? String
                                    self.restaurentAddress.text = "\((prod["address"] as? String)!), \((prod["city"] as? String)!), \(((prod["pincode"] as? String)?.uppercased())!)\n Contact number - \(prod["contact_number"] as! String)"
                                    
                                    let prc = prod["price"] as? String
                                    let qty = prod["quantity"] as? String
                                    self.totPrc =  self.totPrc + (Float(prc!)! * Float(qty!)!)
                                }
                                DispatchQueue.main.async(execute: { () -> Void in
                                    self.cartTableView.reloadData()
                                    if (back == "")
                                    {
                                     self.setScrollView()
                                    }
                                    back = ""
                                })
                            }
                            else if (check == "remove") {
                                //self.productArray = []
                                self.totPrc = 0.00
                                self.viewDidLoad()
                                ApiResponse.alert(title: "Done", message: "Product removed successfully", controller: self)
                            }
                            else {
                                print("qty update")
                            }
                        }
                    }
                    else {
                        OperationQueue.main.addOperation {
                            LoadingIndicatorView.hide()
                            self.productArray = []
                            self.cartTableView.reloadData()
                            self.setScrollView()
                        }
                        ApiResponse.alert(title: "Ooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
            ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }
    
    func setScrollView()
    {
        if (productArray.count  != 0)
        {
            if (productArray.count == 1) {
                 ht = ((productArray.count) * 55)
            }
            else {
                ht = ((productArray.count - 1) * 55)
            }
            tableHtConstraint.constant = (cartTableView.bounds.size.height)+CGFloat(ht)
            
//            if ((placeButton.frame.origin.y + placeButton.bounds.size.height + CGFloat(ht) + 60) > 508)
//            {
                scrollView.isScrollEnabled = true
                contentHtConstraint.constant = cartTableView.frame.origin.y + tableHtConstraint.constant + 75
//            }
//            else
//            {
//                scrollView.isScrollEnabled = false
//                contentHtConstraint.constant = 610
//            }
        }
        else
        {
            cartTableView.isHidden = true
            placeButton.isHidden = true
            
            let lab =  UILabel.init()
            lab.frame.size = CGSize(width : 200, height : 50)
            lab.center = self.view.center
            lab.text = "Your cart is empty"
            lab.textAlignment = NSTextAlignment.center
            lab.font = UIFont(name: "Open Sans", size: 18)
            self.view.addSubview(lab)
        }
    }

    @IBAction func placeOrder(_ sender: Any) {
        
        var jsonArray : [NSDictionary] = []
        var i = 0
        for dict in productArray
        {
            let prc = dict["price"] as? String
            let qty = dict["quantity"] as? String
            
            let dict1 = ["id": dict["id"] as AnyObject , "user_id": dict["user_id"] as AnyObject, "quantity": dict["quantity"] as AnyObject, "product_id": dict["product_id"] as AnyObject, "price": "\(Int(prc!)! * Int(qty!)!)", "product_name" : dict["product_name"] as AnyObject, "restaurant_id" : dict["restaurant_id"] as AnyObject, "image" : dict["image"] as AnyObject, "product_note" : dict["product_note"] as AnyObject] as [String : Any]
            
            jsonArray.append(dict1 as NSDictionary)
            i = i + 1
        }
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonArray, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            theJSONText = NSString(data: jsonData,
                                       encoding: String.Encoding.ascii.rawValue)
            // here "decoded" is of type `Any`, decoded from JSON data
        }
        catch {
            print(error.localizedDescription)
        }
        
        let order = self.storyboard?.instantiateViewController(withIdentifier: "order") as! OrderViewController
        order.itemNumber = productArray.count
        order.totalPrc = "\(totPrc)"
        order.passJsonText = self.theJSONText
        self.present(order, animated: true, completion: nil)
    }
    
    @IBAction func editCart(_ sender: Any) {
        let checkout = self.storyboard?.instantiateViewController(withIdentifier: "checkout") as! CheckoutViewController
        self.present(checkout, animated: true, completion: nil)
    }
    
    @IBAction func onback(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}

class CartTableViewCell : UITableViewCell {
    
    @IBOutlet var prodImage: UIImageView!
    @IBOutlet var productName: UILabel!
    @IBOutlet var qtyLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
}

class TotalTableViewCell: UITableViewCell {
    @IBOutlet var totalQty: UILabel!
    @IBOutlet var totalPrc: UILabel!
}

extension MyCartViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 1)
        {
            return productArray.count
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section == 0)
        {
            let cell = cartTableView.dequeueReusableCell(withIdentifier: "cell")
            
            return cell!
        }
        else if (indexPath.section == 1)
        {
            let cell = cartTableView.dequeueReusableCell(withIdentifier: "cartcell")! as! CartTableViewCell
            
            let dict = productArray[indexPath.row]
            cell.productName.text = dict["product_name"] as? String
            
            let prc = dict["price"] as? String
            let qty = dict["quantity"] as? String
            let totalPrice = Float(prc!)! * Float(qty!)!
            let twoDecimalPlaces1 = String(format: "%.2f", totalPrice)
            cell.priceLabel.text = "\u{00A3}\(twoDecimalPlaces1)"
            cell.qtyLabel.text = dict["quantity"] as? String
            cell.prodImage.sd_setImage(with: NSURL(string : "https://www.sweettoothapp.co.uk/\(dict["image"] as! String)") as URL!)
            
            totalQty = totalQty + Int(cell.qtyLabel.text!)!
            
            return cell
        }
        else
        {
            let cell = cartTableView.dequeueReusableCell(withIdentifier: "totalcell")! as! TotalTableViewCell
            
            cell.totalQty.text = "\(totalQty)"
            let twoDecimalPlaces1 = String(format: "%.2f", totPrc)
            cell.totalPrc.text = "\u{00A3}\(twoDecimalPlaces1)"
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
}

