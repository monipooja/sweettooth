//
//  Constant.swift
//  CallBackDemo
//
//  Created by Prashant Shinde on 3/11/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import Foundation

class Constant
{
    static let Status_Not_200 : String = "statusnot200"
    static let BASE_URL : String = "https://www.sweettoothapp.co.uk/"
    // "https://www.ilearnersindia.com/"
}
