//
//  ProfileViewController.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 3/30/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import SystemConfiguration

class ProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var contactNo: UITextField!
    @IBOutlet var emailId: UITextField!
    @IBOutlet var addressText: UITextField!
    @IBOutlet var countryText: UITextField!
    @IBOutlet var citytext: UITextField!
    @IBOutlet var pincodeText: UITextField!
    
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var changeButton: UIButton!
    
    var tfArray : [UITextField] = []
    var newFrame : CGRect!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        saveButton.layer.cornerRadius = 20
        changeButton.layer.cornerRadius = 20
        
        tfArray = [firstName, lastName, emailId, contactNo, addressText, countryText, citytext, pincodeText]
        let placeholder = ["First Name", "Last Name", "Email Id", "Mobile no", "Address", "County", "City/Town", "Postcode"]
        var i = 0
        
        for tf in tfArray
        {
            tf.delegate = self
            tf.isUserInteractionEnabled = false
            
            let paddingForFirst = UIView(frame:  CGRect(x: 0, y: 0, width: 10, height: tf.frame.size.height))
            //Adding the padding to the second textField
            tf.leftView = paddingForFirst
            tf.leftViewMode = UITextFieldViewMode .always
            
            tf.attributedPlaceholder = NSAttributedString(string:placeholder[i],
                                                          attributes:[NSForegroundColorAttributeName: UIColor.white])
            i += 1
        }
        
        let datadict = user.object(forKey: "logindata") as! NSDictionary
        firstName.text = datadict["first_name"] as? String
        lastName.text = datadict["last_name"] as? String
        contactNo.text = datadict["contact_no"] as? String
        emailId.text = datadict["email"] as? String
        addressText.text = datadict["address"] as? String
        countryText.text = datadict["country"] as? String
        citytext.text = datadict["city"] as? String
        pincodeText.text = (datadict["pincode"] as? String)?.uppercased()
    }
    
    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if (textField.placeholder == "County") || (textField.placeholder == "City/Town") || (textField.placeholder == "Postcode")
        {
            self.newFrame = self.view.frame
            
            newFrame.origin.y = self.view.bounds.origin.y - 100
            newFrame.size.height = self.view.bounds.size.height
            
            UIView.animate(withDuration: 0.25, animations:
                {() -> Void in
                    
                    self.view.frame = self.newFrame
            })
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if (textField.placeholder == "County") || (textField.placeholder == "City/Town") || (textField.placeholder == "Postcode")
        {
            newFrame.origin.y = 0
            newFrame.size.height = self.view.bounds.size.height
            
            UIView.animate(withDuration: 0.25, animations:
                {() -> Void in
                    self.view.frame = self.newFrame
            })
        }
    }
    
    @IBAction func saveUpdate(_ sender: Any) {
        
       if (saveButton.titleLabel?.text == "EDIT") {
            saveButton.setTitle("SAVE", for: .normal)
            for tf in tfArray
            {
                tf.isUserInteractionEnabled = true
                emailId.isUserInteractionEnabled = false
            }
        }
        else {
            
            if (firstName.text == "") || (lastName.text == "") || (contactNo.text == "") || (emailId.text == "") || (addressText.text == "") || (citytext.text == "") || (pincodeText.text == "") {
                ApiResponse.alert(title: "Blank", message: "Fields cannot be blanks", controller: self)
            }
            else if ((contactNo.text?.characters.count)! < 11) || ((contactNo.text?.characters.count)! > 11) {
                ApiResponse.alert(title: "Ooops", message: "Mobile No. is not recognised as a UK number", controller: self)
            }
            else if (ApiResponse.validateEmail(emailId.text!) == false) {
                ApiResponse.alert(title: "Invalid Email", message: "Please enter valid email id", controller: self)
            }
            else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
            {
                LoadingIndicatorView.show()
         
                firstName.text = firstName.text?.uppercaseFirst
                lastName.text = lastName.text?.uppercaseFirst
                emailId.text = emailId.text?.lowercased()
                addressText.text = addressText.text?.uppercaseFirst
                countryText.text = countryText.text?.uppercaseFirst
                citytext.text = citytext.text?.uppercaseFirst
                pincodeText.text = pincodeText.text?.uppercased()
                
                let userid = (user.object(forKey: "logindata") as! NSDictionary)["user_id"] as! String
                
                let params = "first_name=\(firstName.text!)&last_name=\(lastName.text!)&contact_no=\(contactNo.text!)&email=\(emailId.text!)&address=\(addressText.text!)&pincode=\(pincodeText.text!)&city=\(citytext.text!)&state=NA&country=\(countryText.text!)&image=&type=ios&user_id=\(userid)"
                
                ApiResponse.onResponsePostPhp(url: "home/updateprofileuser", parms: params, completion: { (result, error) in
                    
                    if (error == "") {
                        print("upadte result = \(result)")
                        
                        let status = result["status"] as! Bool
                        let message = result["message"] as! String
                        if (status == true) {
                            OperationQueue.main.addOperation {
                                
                                ApiResponse.alert(title: "Done", message: message, controller: self)
                                self.saveButton.setTitle("EDIT", for: .normal)
                                for tf in self.tfArray
                                {
                                    tf.isUserInteractionEnabled = false
                                }
                                user.set((result["data"] as! NSDictionary), forKey: "logindata")
                                user.synchronize()
                                self.viewDidLoad()
                            }
                        }
                        else {
                            ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                        }
                    }
                    else {
                        if (error == Constant.Status_Not_200) {
                            ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                        }
                        else {
                            ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                        }
                    }
                })
            }
            else {
                ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
            }
        }
    }
    
    @IBAction func changePassword(_ sender: Any) {
        let change = self.storyboard?.instantiateViewController(withIdentifier: "change") as! ChangePasswordVC
        self.present(change, animated: true, completion: nil)
    }
    
    @IBAction func openDashboard(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.add111()
    }
    
}
