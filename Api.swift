//
//  Api.swift
//  CallBackDemo
//
//  Created by Prashant Shinde on 3/11/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import Foundation
import UIKit

class ApiResponse {
    
    static func onResponsePost(url: String,parms: NSDictionary, completion: @escaping (_ res:NSDictionary , _ error : String) -> Void) {
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let url = NSURL(string:"\(Constant.BASE_URL)\(url)")
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parms, options: JSONSerialization.WritingOptions())
            
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        completion(["":""], Constant.Status_Not_200)
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    completion(["":""], "\(String(describing: error))")
                    return
                }
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary
                    print("result = \(String(describing: result))")
                    completion(result! , "")
                }
                catch
                {
                }
            }
            task.resume()
        }
        catch
        {
        }
    }
    
    static func onResponseGet(url: String, completion: @escaping (_ res:NSDictionary , _ error : String) -> Void) {
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        
        let url = NSURL(string:"\(Constant.BASE_URL)\(url)")
        let request = NSMutableURLRequest(url: url! as URL)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "GET"
        
        do {
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        completion(["":""], Constant.Status_Not_200)
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    completion(["":""], "\(String(describing: error))")
                    return
                }
                
                do {
                    let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary
                    print("result = \(String(describing: result))")
                    completion(result! , "")
                }
                catch
                {
                }
            }
            task.resume()
        }
    }
    
    static func onResponsePostPhp(url: String,parms: String, completion: @escaping (_ res:NSDictionary , _ error : String) -> Void) {
        
        var request = URLRequest(url: URL(string: "\(Constant.BASE_URL)\(url)")!)
        request.httpMethod = "POST"
        let postString = parms
        //print("post string = \(postString)")
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(String(describing: error))")
                completion(["":""], "\(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                completion(["":""], Constant.Status_Not_200)
                return
            }
            
            if let parsedData = try? JSONSerialization.jsonObject(with: data) as! [String:Any]
            {
                //print("parsed data = \(parsedData)")
                completion(parsedData as NSDictionary , "")
            }
            else
            {
                OperationQueue.main.addOperation {
                    LoadingIndicatorView.hide()
                    print("false")
                }
            }
        }
        task.resume()
    }

    static func onResponseGetPhp(url: String, completion: @escaping (_ res:NSDictionary , _ error : String) -> Void)
    {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let url = NSURL(string:"\(Constant.BASE_URL)\(url)")
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "GET"
        
        do {
            let task = session.dataTask(with: request as URLRequest) {
                data, response, error in
                
                if let httpResponse = response as? HTTPURLResponse {
                    if httpResponse.statusCode != 200 {
                        print("response was not 200: \(String(describing: response))")
                        completion(["":""], Constant.Status_Not_200)
                        return
                    }
                }
                if (error != nil) {
                    print("error submitting request: \(String(describing: error))")
                    completion(["":""], "\(String(describing: error))")
                    return
                }
                do
                {
                    let result = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions()) as? NSDictionary
                     print("result1 = \(String(describing: result))")
                     completion(result!, "")
                }
                catch
                {
                }
            }
            task.resume()
        }
    }
        
  static func alert(title: String, message : String , controller: UIViewController)
    {
      OperationQueue.main.addOperation
        {
            LoadingIndicatorView.hide()
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    static func CameraGallery(controller: UIViewController, imagePicker : UIImagePickerController)
    {
        let actionSheetController : UIAlertController = UIAlertController(title: "Please select", message: "Option to select", preferredStyle: .actionSheet)
        
        let cameraActionButton: UIAlertAction = UIAlertAction(title: "Camera", style: .default)
        { action -> Void in
            print("Camera")
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                controller.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(cameraActionButton)
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: "Gallery", style: .default)
        { action -> Void in
            print("Gallery")
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)
            {
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = false
                controller.present(imagePicker, animated: true, completion: nil)
            }
        }
        actionSheetController.addAction(galleryActionButton)
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        
        controller.present(actionSheetController, animated: true, completion: nil)
    }
    
    static func resize(_ image: UIImage, maxHt : Float, maxWd : Float) -> UIImage
    {
        var actualHeight: Float = Float(image.size.height)
        var actualWidth: Float = Float(image.size.width)
        let maxHeight: Float = maxHt       //300.0
        let maxWidth: Float = maxWd        //400.0
        var imgRatio: Float = actualWidth / actualHeight
        let maxRatio: Float = maxWidth / maxHeight
        //var compressionQuality: Float = 0.5
        //50 percent compression
        if actualHeight > maxHeight || actualWidth > maxWidth {
            if imgRatio < maxRatio {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            }
            else if imgRatio > maxRatio {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            }
        }
        let rect = CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(actualWidth), height: CGFloat(actualHeight))
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData = UIImagePNGRepresentation(img)
        UIGraphicsEndImageContext()
        return UIImage(data: imageData!)!
    }
    
    static func validateEmail(_ emailStr : String) -> Bool
    {
        let a = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" as String
        let emailTest = NSPredicate(format: "SELF MATCHES %@", a)
        return emailTest.evaluate(with: emailStr)
    }
    
    static func calculateHeightForString(_ inString:String) -> CGFloat
    {
        let messageString = inString
        let attributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 13.0)]
        let attrString:NSAttributedString? = NSAttributedString(string: messageString, attributes: attributes)
        
        let rect:CGRect = attrString!.boundingRect(with: CGSize(width : 300.0, height : CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, context:nil )   //hear u will get nearer height not the exact value
        let requredSize:CGRect = rect
        return requredSize.height         //requredSize.height  //to include button's in your tableview
    }
    
    static func callRating(_ ratingValue : Double, starRating : [UIImageView])
    {
        if((ratingValue >= 0.5) && (ratingValue < 1.0))
        {
            starRating[0].image = (UIImage (named: "starHalf.png"))
            starRating[1].image = (UIImage (named: "starGray.png"))
            starRating[2].image = (UIImage (named: "starGray.png"))
            starRating[3].image = (UIImage (named: "starGray.png"))
            starRating[4].image = (UIImage (named: "starGray.png"))
        }
        else if((ratingValue >= 1.0) && (ratingValue < 1.5))
        {
            starRating[0].image = (UIImage (named: "starPink.png"))
            starRating[1].image = (UIImage (named: "starGray.png"))
            starRating[2].image = (UIImage (named: "starGray.png"))
            starRating[3].image = (UIImage (named: "starGray.png"))
            starRating[4].image = (UIImage (named: "starGray.png"))
        }
        else if((ratingValue >= 1.5) && (ratingValue < 2.0))
        {
            starRating[0].image = (UIImage (named: "starPink.png"))
            starRating[1].image = (UIImage (named: "starHalf.png"))
            starRating[2].image = (UIImage (named: "starGray.png"))
            starRating[3].image = (UIImage (named: "starGray.png"))
            starRating[4].image = (UIImage (named: "starGray.png"))
        }
        else if((ratingValue >= 2.0) && (ratingValue < 2.5))
        {
            starRating[0].image = (UIImage (named: "starPink.png"))
            starRating[1].image = (UIImage (named: "starPink.png"))
            starRating[2].image = (UIImage (named: "starGray.png"))
            starRating[3].image = (UIImage (named: "starGray.png"))
            starRating[4].image = (UIImage (named: "starGray.png"))
        }
        else if((ratingValue >= 2.5) && (ratingValue < 3.0))
        {
            starRating[0].image = (UIImage (named: "starPink.png"))
            starRating[1].image = (UIImage (named: "starPink.png"))
            starRating[2].image = (UIImage (named: "starHalf.png"))
            starRating[3].image = (UIImage (named: "starGray.png"))
            starRating[4].image = (UIImage (named: "starGray.png"))
            
        }
        else if((ratingValue >= 3.0) && (ratingValue < 3.5))
        {
            starRating[0].image = (UIImage (named: "starPink.png"))
            starRating[1].image = (UIImage (named: "starPink.png"))
            starRating[2].image = (UIImage (named: "starPink.png"))
            starRating[3].image = (UIImage (named: "starGray.png"))
            starRating[4].image = (UIImage (named: "starGray.png"))
        }
        else if((ratingValue >= 3.5) && (ratingValue < 4.0))
        {
            starRating[0].image = (UIImage (named: "starPink.png"))
            starRating[1].image = (UIImage (named: "starPink.png"))
            starRating[2].image = (UIImage (named: "starPink.png"))
            starRating[3].image = (UIImage (named: "starHalf.png"))
            starRating[4].image = (UIImage (named: "starGray.png"))
        }
        else if((ratingValue >= 4.0) && (ratingValue < 4.5))
        {
            starRating[0].image = (UIImage (named: "starPink.png"))
            starRating[1].image = (UIImage (named: "starPink.png"))
            starRating[2].image = (UIImage (named: "starPink.png"))
            starRating[3].image = (UIImage (named: "starPink.png"))
            starRating[4].image = (UIImage (named: "starGray.png"))
        }
        else if((ratingValue >= 4.5) && (ratingValue < 5.0))
        {
            starRating[0].image = (UIImage (named: "starPink.png"))
            starRating[1].image = (UIImage (named: "starPink.png"))
            starRating[2].image = (UIImage (named: "starPink.png"))
            starRating[3].image = (UIImage (named: "starPink.png"))
            starRating[4].image = (UIImage (named: "starHalf.png"))
        }
        else if((ratingValue == 5.0))
        {
            for i in 0...4
            {
                starRating[i].image = (UIImage (named: "starPink.png"))
            }
        }
    }

}
