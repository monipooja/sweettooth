//
//  ChangePasswordVC.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 4/4/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import SystemConfiguration

class ChangePasswordVC: UIViewController, UITextFieldDelegate {

    
    @IBOutlet var oldPassword: UITextField!
    @IBOutlet var newPassword: UITextField!
    @IBOutlet var confirmPassword: UITextField!
    
    @IBOutlet var savebutton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        savebutton.layer.cornerRadius = 20.0
        
        let tfArray : [UITextField] = [oldPassword, newPassword, confirmPassword]
        let placeholder = ["Old Password", "New Password", "Confirm Password"]
        var i = 0
        
        for tf in tfArray
        {
            tf.delegate = self
            let paddingForFirst = UIView(frame:  CGRect(x: 0, y: 0, width: 5, height: tf.frame.size.height))
            //Adding the padding to the second textField
            tf.leftView = paddingForFirst
            tf.leftViewMode = UITextFieldViewMode .always
            
            tf.attributedPlaceholder = NSAttributedString(string:placeholder[i],
                                                          attributes:[NSForegroundColorAttributeName: UIColor.white])
            i += 1
        }
    }
    
    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func savePassword(_ sender: Any) {
        
        if (oldPassword.text == "") || (newPassword.text == "") || (confirmPassword.text == "")
        {
            ApiResponse.alert(title: "Empty", message: "Please fill all the fields", controller: self)
        }
        else if ((oldPassword.text?.characters.count)! < 6) {
            ApiResponse.alert(title: "Ooops", message: "Password should be minimum 6 characters", controller: self)
        }
        else if ((newPassword.text?.characters.count)! < 6) {
            ApiResponse.alert(title: "Ooops", message: "Password should be minimum 6 characters", controller: self)
        }
        else if (newPassword.text! != confirmPassword.text!) {
            ApiResponse.alert(title: "Password Mismatch", message: "Password confirmation does not match", controller: self)
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN {
            
            LoadingIndicatorView.show()
            
            let userid = (user.object(forKey: "logindata") as! NSDictionary)["user_id"] as! String
            let params = "oldpassword=\(oldPassword.text!)&newpassword=\(newPassword.text!)&user_id=\(userid)"
            
            ApiResponse.onResponsePostPhp(url: "home/changepassword", parms: params, completion: { (result, error) in
                
                if (error == "") {
                    print("password result = \(result)")
                    
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    
                    if (status == true) {
                        
                        OperationQueue.main.addOperation {
                            LoadingIndicatorView.hide()
                            let alert = UIAlertController(title: "Done", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: { action in
                                self.oldPassword.text = ""
                                self.newPassword.text = ""
                                self.confirmPassword.text = ""
                                self.dismiss(animated: false, completion: nil)
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    else {
                        ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
            ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

}
