//
//  PaymentViewController.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 4/5/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import Stripe
import SystemConfiguration

class PaymentViewController: UIViewController, UITextFieldDelegate, STPPaymentCardTextFieldDelegate, PayPalPaymentDelegate {

    @IBOutlet var creditButton: UIButton!
    @IBOutlet var cashButton: UIButton!
    @IBOutlet var paypalButton: UIButton!
    @IBOutlet var cashPayButton: UIButton!
   
    @IBOutlet var payview: UIView!
    @IBOutlet var cardFirstName: UITextField!
    @IBOutlet var cardNumber: UITextField!
    @IBOutlet var cvvNumber: UITextField!
    @IBOutlet var payNowButtton: UIButton!
    @IBOutlet var monthButton: UIButton!
    @IBOutlet var yearButton: UIButton!
    @IBOutlet var expiryTable: UITableView!
    @IBOutlet var saveButton: UIButton!
    
    @IBOutlet var cashHtConstraint: NSLayoutConstraint!
    @IBOutlet var cardHtConstraint: NSLayoutConstraint!
    @IBOutlet var bgimageHtConstraint: NSLayoutConstraint!
    @IBOutlet var scrollVw: UIScrollView!
    
    // Your Order Outlets
    @IBOutlet var orderTableVw: UITableView!
    @IBOutlet var orderHtConstraint: NSLayoutConstraint!
    
    var btnArray : [UIButton] = []
    var titleStr : String = ""
    var userid : String = ""
    var priceStr : String = ""
    var tableStr : String = ""
    var maxYear, currentYear : String!
    var yearArray : [String] = []
    var saveValue : Bool = false
    var newFrame : CGRect!
    var totalItemCount : Int = 0
    var tableName : String = ""
    var ht : Int = 0
    var passArray : [NSDictionary] = []
    
    var resultText = "" // empty
    var payPalConfig = PayPalConfiguration() // default
    
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        payview.isHidden = true
        payview.layer.cornerRadius = 4.0
        
        expiryTable.layer.borderWidth = 1.0
        expiryTable.layer.borderColor = UIColor.lightGray.cgColor
        
        cashPayButton.isHidden = true
        cashHtConstraint.constant = 0
        cardHtConstraint.constant = 0
        bgimageHtConstraint.constant = cashPayButton.frame.origin.y + cashPayButton.frame.height + 20
        scrollVw.isScrollEnabled = true
        
        btnArray = [creditButton, cashButton, paypalButton]
        for btn in btnArray
        {
            btn.layer.masksToBounds = false
            btn.layer.cornerRadius = btn.bounds.size.width/2.0
            btn.clipsToBounds = true
            
            btn.layer.borderWidth = 1.0
            btn.layer.borderColor = UIColor.white.cgColor
        }
        
        monthButton.layer.borderColor = UIColor.lightGray.cgColor
        monthButton.layer.borderWidth = 0.5
        monthButton.layer.cornerRadius = 4.0
        yearButton.layer.borderColor = UIColor.lightGray.cgColor
        yearButton.layer.borderWidth = 0.5
        yearButton.layer.cornerRadius = 4.0
        
        monthButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 0.0)
        yearButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 0.0)
        
        cashPayButton.layer.cornerRadius = 20.0
    
        payNowButtton.layer.cornerRadius = 20.0
        payNowButtton.setTitle("PAY \(priceStr)", for: .normal)
        
        let textArray : [UITextField] = [cardFirstName, cardNumber, cvvNumber]
        for textf in textArray
        {
            textf.delegate = self
            let paddingForFirst = UIView(frame:  CGRect(x: 0, y: 0, width: 5, height: textf.frame.size.height))
            //Adding the padding to the second textField
            textf.leftView = paddingForFirst
            textf.leftViewMode = UITextFieldViewMode .always
        }
        
        userid = (user.object(forKey: "logindata") as! NSDictionary)["user_id"] as! String
        
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let currentDate: NSDate = NSDate()
        let components: NSDateComponents = NSDateComponents()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy"
        currentYear = "\(dateFormatter.string(from: currentDate as Date))"
        yearArray.append(currentYear)
        
        for i in 1...30
        {
            components.year = +i
            
            let maxDate: NSDate = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options(rawValue: 0))! as NSDate
            
            maxYear = "\(dateFormatter.string(from: maxDate as Date))"
            yearArray.append(maxYear)
        }
        
        payPalConfig.acceptCreditCards = false
        payPalConfig.merchantName = "Sweet Tooth"
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        
        payPalConfig.payPalShippingAddressOption = .payPal;
        
        orderTableVw.separatorStyle = UITableViewCellSeparatorStyle.none
        orderTableVw.layer.borderWidth = 1.0
        orderTableVw.layer.borderColor = UIColor.white.cgColor
        
        self.setScrollView()
        
       // print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
    }
    
    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        PayPalMobile.preconnect(withEnvironment: environment)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        self.newFrame = self.view.frame
        newFrame.origin.y = self.view.frame.origin.y - 100
        newFrame.size.height = self.view.bounds.size.height
        
        UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                self.view.frame = self.newFrame
        })
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        newFrame.origin.y = 0
        newFrame.size.height = self.view.bounds.size.height
        UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                self.view.frame = self.newFrame
        })
    }
    
    func setScrollView()
    {
        if (totalItemCount  != 0)
        {
            if (totalItemCount == 1) {
                ht = ((totalItemCount) * 50) + 259
            }
            else {
                ht = ((totalItemCount - 1) * 50) + 259
            }
            orderTableVw.frame.size = CGSize(width : orderTableVw.bounds.size.width, height : CGFloat(ht) + 50)
            
            orderHtConstraint.constant = CGFloat(ht) + 50
            
            if ((CGFloat(ht) + 50) > 360)
            {
                scrollVw.isScrollEnabled = true
                bgimageHtConstraint.constant = orderTableVw.frame.origin.y + orderTableVw.frame.height + 100
            }
            else
            {
                scrollVw.isScrollEnabled = false
                bgimageHtConstraint.constant = 607 //orderTableVw.frame.origin.y + orderTableVw.frame.height + 100
            }
        }
    }
    
    @IBAction func creditCard(_ sender: Any) {
        self.selectCircle(0)
        titleStr = "Stripe"
        
        let dictCard = user.object(forKey: "savecard") as? NSDictionary
        let keyExists = dictCard?["save"] != nil
        //print("dictttttt = \(dictCard),,,,,,\(keyExists)")
        if (keyExists == true) || (dictCard != nil)
        {
            let checkValue = dictCard?["save"] as! Bool
            if (checkValue == true)
            {
                cardNumber.text = dictCard?["card_number"] as? String
                cardFirstName.text = dictCard?["holder_name"] as? String
                monthButton.setTitle(dictCard?["exp_month"] as! String?, for: .normal)
                yearButton.setTitle(dictCard?["exp_year"] as! String?, for: .normal)
                
                saveButton.setBackgroundImage(UIImage(named : "32.png"), for: .normal)
                saveButton.setImage(UIImage(named : "33.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
                saveValue = true
            }
        }
        else {
            cardNumber.text = ""
            cardFirstName.text = ""
            cvvNumber.text = ""
            monthButton.setTitle("MM", for: .normal)
            yearButton.setTitle("YYYY", for: .normal)
            
            saveButton.setBackgroundImage(UIImage(named : "32.png"), for: .normal)
            saveButton.setImage(UIImage(named : ""), for: .normal)
            saveValue = false
        }
        
        payview.isHidden = false
        cashPayButton.isHidden = true
        cashHtConstraint.constant = 0
        cardHtConstraint.constant = 432
        //print("hhhhhhhhh = \(orderTableVw.frame.origin.y) *** \(orderTableVw.frame.height)")

        bgimageHtConstraint.constant = 643 + orderTableVw.frame.height + 100
        scrollVw.isScrollEnabled = true
    }
    
    @IBAction func cashOnDelivery(_ sender: Any) {
        self.selectCircle(1)
        titleStr = "COD"
        
        payview.isHidden = true
        cashPayButton.isHidden = false
        cashHtConstraint.constant = 40
        cardHtConstraint.constant = 0
        print("hhhhhhhhhhhhhhh = \((CGFloat(ht) + 50))****\(orderTableVw.frame.origin.y)")
        if ((CGFloat(ht) + 50) > 340)
        {
            scrollVw.isScrollEnabled = true
            bgimageHtConstraint.constant = 211 + orderTableVw.frame.height + 100
        }
        else {
            scrollVw.isScrollEnabled = false
            bgimageHtConstraint.constant = 607
        }
        cashPayButton.setTitle("PLACE MY ORDER", for: .normal)
    }
    
    func selectCircle(_ indexValue : Int)
    {
        var i = 0
        for btn in btnArray
        {
            if (i == indexValue) {
                btn.backgroundColor = UIColor.init(red: 250/255.0, green: 103/255.0, blue: 194/255.0, alpha: 1)
            }
            else {
                btn.backgroundColor = UIColor.clear
            }
            i = i + 1
        }
    }
    
    @IBAction func openPayview(_ sender: Any) {
        
        if (titleStr == "") {
            ApiResponse.alert(title: "Oooops", message: "Please select payment method", controller: self)
        }
        else if (titleStr == "COD"){
            let parameters = "user_id=\(userid)&paymenttype=COD"
            callService("home/paymenttype", params: parameters, check: "cash")
        }
        else {
            
            // Optional: include payment details
            let finalPrc = priceStr.replacingOccurrences(of: "\u{00A3}", with: "")
            let subtotal = NSDecimalNumber(string : finalPrc)
            let shipping = NSDecimalNumber(string: "0.00")
            let tax = NSDecimalNumber(string: "0.00")
            //let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
            
            let total = subtotal.adding(shipping).adding(tax)
            
            let payment = PayPalPayment(amount: total, currencyCode: "GBP", shortDescription: "\(totalItemCount) Items", intent: .sale)
            
            if (payment.processable) {
                let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
                present(paymentViewController!, animated: true, completion: nil)
            }
            else {
                // This particular payment will always be processable. If, for
                // example, the amount was negative or the shortDescription was
                // empty, this payment wouldn't be processable, and you'd want
                // to handle that here.
                print("Payment not processalbe: \(payment)")
            }
        }
    }
    
    @IBAction func selectMonth(_ sender: Any) {
        tableStr = "mm"
        expiryTable.dataSource = self
        expiryTable.delegate = self
        expiryTable.reloadData()
        expiryTable.frame = CGRect(x : 15, y : 57, width : expiryTable.bounds.width, height : expiryTable.bounds.height)
        expiryTable.isHidden = false
    }
    
    @IBAction func selectYear(_ sender: Any) {
        tableStr = "yy"
        expiryTable.dataSource = self
        expiryTable.delegate = self
        expiryTable.reloadData()
        expiryTable.frame = CGRect(x : 90, y : 57, width : expiryTable.bounds.width, height : expiryTable.bounds.height)
        expiryTable.isHidden = false
    }
    
    @IBAction func payNow(_ sender: Any) {
        
        if(cardFirstName.text == "") || (cardNumber.text == "") || (cvvNumber.text == "") {
            ApiResponse.alert(title: "Ooops", message: "Please enter card details", controller: self)
        }
        else if (monthButton.titleLabel?.text == "MM") {
            ApiResponse.alert(title: "Ooops", message: "Please select month", controller: self)
        }
        else if (yearButton.titleLabel?.text == "YYYY") {
            ApiResponse.alert(title: "Ooops", message: "Please select year", controller: self)
        }
        else if ((cardNumber.text?.characters.count)! < 16) || ((cardNumber.text?.characters.count)! > 16){
            ApiResponse.alert(title: "Ooops", message: "Card number is invalid ", controller: self)
        }
        else if ((cvvNumber.text?.characters.count)! < 3) || ((cvvNumber.text?.characters.count)! > 3){
            ApiResponse.alert(title: "Ooops", message: "CVV is invalid ", controller: self)
        }
        else
        {
            let CardDict = ["card_number" : cardNumber.text!, "holder_name" : cardFirstName.text!, "exp_month" : monthButton.titleLabel?.text as Any, "exp_year" : yearButton.titleLabel?.text as Any, "save" : saveValue] as [String : Any]
            user.set(CardDict, forKey: "savecard")
            user.synchronize()
            
            let stripCard = STPCardParams()
            
            // Split the expiration date to extract Month & Year
            let expMonth = monthButton.titleLabel?.text
            let expYear = yearButton.titleLabel?.text
            
            // Send the card info to Strip to get the token
            stripCard.number = self.cardNumber.text
            stripCard.cvc = self.cvvNumber.text
            stripCard.expMonth = UInt(expMonth!)!
            stripCard.expYear = UInt(expYear!)!
            
            if STPCardValidator.validationState(forCard: stripCard) == .invalid
            {
                ApiResponse.alert(title: "Invalid", message: "Your card is invalid", controller: self)
            }
            else {
                LoadingIndicatorView.show()
                let parameters = "user_id=\(userid)&name=\(cardFirstName.text!)&paymenttype=strip&cardnumber=\(cardNumber.text!)&exp_month=\(expMonth!)&exp_year=\(expYear!)&cvv=\(cvvNumber.text!)"
                print("stripe parameters =\(parameters)")
                self.callService("home/addorderspaymentdetails", params: parameters, check: "pay")
            }
        }
    }
        
    func callService(_ urlStr : String, params : String,  check : String) {
        
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN {
            ApiResponse.onResponsePostPhp(url: urlStr, parms: params, completion: { (result, error) in
                
                if (error == "") {
                    
                    print("payment result = \(result)")
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                   
                    if (status == true) {
                        
                        OperationQueue.main.addOperation {
                            LoadingIndicatorView.hide()
                            let data = result["data"] as! NSDictionary
                            user.set("0", forKey: "itemcount")
                            user.synchronize()
                            if (check == "cash") {
                                let bill = self.storyboard?.instantiateViewController(withIdentifier: "billing") as! BillingViewController
                                bill.passDict = data
                                bill.orderArray = self.passArray
                                self.present(bill, animated: true, completion: nil)
                            }
                            else if (check == "pay"){
                               // pay
                                ApiResponse.alert(title: "Done", message: message, controller: self)
                                let bill = self.storyboard?.instantiateViewController(withIdentifier: "billing") as! BillingViewController
                                bill.passDict = data
                                bill.orderArray = self.passArray
                                self.present(bill, animated: true, completion: nil)
                            }
                            else {
                                ApiResponse.alert(title: "Done", message: message, controller: self)
                                let bill = self.storyboard?.instantiateViewController(withIdentifier: "billing") as! BillingViewController
                                bill.passDict = data
                                bill.orderArray = self.passArray
                                self.present(bill, animated: true, completion: nil)
                            }
                        }
                    }
                    else {
                        ApiResponse.alert(title: "Ooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
            ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }
    
    @IBAction func saveCard(_ sender: Any) {
        if (saveValue == false)
        {
            if(cardFirstName.text == "") || (cardNumber.text == "") {
                ApiResponse.alert(title: "Ooops", message: "Please enter card details", controller: self)
            }
            else if ((cardNumber.text?.characters.count)! < 16) {
                ApiResponse.alert(title: "Ooops", message: "Card number is invalid ", controller: self)
            }
            else if (monthButton.titleLabel?.text == "MM") {
                ApiResponse.alert(title: "Ooops", message: "Please select month", controller: self)
            }
            else if (yearButton.titleLabel?.text == "YYYY") {
                ApiResponse.alert(title: "Ooops", message: "Please select year", controller: self)
            }
            else {
                let CardDict = ["card_number" : cardNumber.text!, "holder_name" : cardFirstName.text!, "exp_month" : monthButton.titleLabel?.text as Any, "exp_year" : yearButton.titleLabel?.text as Any, "save" : saveValue] as [String : Any]
                user.set(CardDict, forKey: "savecard")
                user.synchronize()
                
                saveButton.setBackgroundImage(UIImage(named : "32.png"), for: .normal)
                saveButton.setImage(UIImage(named : "33.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
                saveValue = true
            }
        }
        else
        {
            user.set(nil, forKey: "savecard")
            user.synchronize()
            
            saveButton.setBackgroundImage(UIImage(named : "32.png"), for: .normal)
            saveButton.setImage(UIImage(named : ""), for: .normal)
            saveValue = false
        }
    }
    
    @IBAction func payWithPayPal(_ sender: Any) {
        
        self.selectCircle(2)
        titleStr = "paypal"
        
        payview.isHidden = true
        cashPayButton.isHidden = false
        cashHtConstraint.constant = 40
        cardHtConstraint.constant = 0
        if ((CGFloat(ht) + 50) > 340)
        {
            scrollVw.isScrollEnabled = true
            bgimageHtConstraint.constant = 211 + orderTableVw.frame.height + 100
        }
        else {
            scrollVw.isScrollEnabled = false
            bgimageHtConstraint.constant = 607
        }
        cashPayButton.setTitle("PAY WITH PAYPAL", for: .normal)
    }
    
    // PayPalPaymentDelegate
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        //print("PayPal Payment Cancelled")
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment)
    {
        //print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
          //  print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.\n\(PayPalPayment.description())")
            
            let data = completedPayment.confirmation[AnyHashable("response")] as! NSDictionary
            let client = completedPayment.confirmation[AnyHashable("client")] as! NSDictionary
            let responsetype = completedPayment.confirmation[AnyHashable("response_type")] as! String
            
            let PayDetails : NSDictionary = ["amount": "\(completedPayment.amount)",
                "currency_code": "\(completedPayment.currencyCode)",
                "short_description": "\(completedPayment.shortDescription)",
                "intent": "\(completedPayment.intent)"]
            
            var jsonArray : NSDictionary = [:]
            jsonArray = ["response" : data, "client" : client, "response_type" : responsetype]
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: jsonArray, options: .prettyPrinted)
                let jsonDetails = try JSONSerialization.data(withJSONObject: PayDetails, options: .prettyPrinted)
                // here "jsonData" is the dictionary encoded in JSON data
                
                let theJSONData = NSString(data: jsonData,
                                           encoding: String.Encoding.ascii.rawValue)
                
                let theJSONDetails = NSString(data: jsonDetails,
                                              encoding: String.Encoding.ascii.rawValue)
                // here "decoded" is of type `Any`, decoded from JSON data
                
                LoadingIndicatorView.show()
                let parameters = "user_id=\(self.userid)&data=\(theJSONData!)&paymentdetails=\(theJSONDetails!)"
                self.callService("home/paypalpay", params: parameters, check: "success")
            }
            catch {
                print(error.localizedDescription)
            }
        })
    }
    
    @IBAction func onBack(_ sender: Any) {
        payview.isHidden = false
        self.dismiss(animated: false, completion: nil)
    }
    
}

extension PaymentViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if (tableStr == "mm") {
            return 1
        }
        else if (tableStr == "yy") {
            return 1
        }
        else {
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableStr == "mm") {
            return 12
        }
        else if (tableStr == "yy") {
            return yearArray.count
        }
        else {
            if (section == 1)
            {
                return passArray.count
            }
            else {
                return 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (tableStr == "mm") {
            let cell : UITableViewCell = expiryTable.dequeueReusableCell(withIdentifier: "mycell")!
            
            cell.textLabel?.font = UIFont(name: "Open Sans", size: 13)
            
            if (indexPath.row < 9) {
                 cell.textLabel?.text = "0\(indexPath.row + 1)"
            }
            else {
                 cell.textLabel?.text = "\(indexPath.row + 1)"
            }
            return cell
        }
        else if (tableStr == "yy") {
            let cell : UITableViewCell = expiryTable.dequeueReusableCell(withIdentifier: "mycell")!
            
            cell.textLabel?.font = UIFont(name: "Open Sans", size: 13)
            
            cell.textLabel?.text = yearArray[indexPath.row]
            
            return cell
        }
        else {
            if (indexPath.section == 0) {
                let cell : UITableViewCell = orderTableVw.dequeueReusableCell(withIdentifier: "ordercell")!
                
                let dict = passArray[indexPath.row]
                cell.textLabel?.text = "Your order from \(dict["restaurant_name"] as! String)"
                cell.textLabel?.textAlignment = .center
                cell.textLabel?.textColor = UIColor.white
                cell.textLabel?.font = UIFont.systemFont(ofSize: 14.0)
                cell.textLabel?.numberOfLines = 0
                
                return cell
            }
            else if (indexPath.section == 1) {
                let cell : Order1TableViewCell = orderTableVw.dequeueReusableCell(withIdentifier: "order1cell") as! Order1TableViewCell
                
                let dict = passArray[indexPath.row]
                cell.itemName.text = "\(dict["product_name"] as! String) (\(dict["quantity"] as! String))"
                cell.itemPrice.text = "\u{00A3}\(dict["price"] as! String)"
                
                return cell
            }
            else {
                let cell : Order2TableViewCell = orderTableVw.dequeueReusableCell(withIdentifier: "order2cell") as! Order2TableViewCell
                
                let dict = passArray[indexPath.row]
                
                cell.subTotal.text = "\(priceStr)"
                let twoDecimalPlaces1 = String(format: "%.2f", Float((dict["delivery_fee"] as! String))!)
                cell.deliveryFee.text = "\u{00A3}\(twoDecimalPlaces1)"
                let sss = priceStr.replacingOccurrences(of: "\u{00A3}", with: "")
                let ps = Float(sss)
                let ds =  Float(dict["delivery_fee"] as! String)
                print("psss = \(String(describing: ps)) ** dsss = \(String(describing: ds))")
                cell.totalPrice.text = "\u{00A3}\(String(format: "%.2f", (ps! + ds!)))"
                cell.nameLabel.text = "\(dict["first_name"] as! String)"
            
                let dlvryType = dict["product_delivery_type"] as? String
                
                if (dlvryType == "Delivery") || (dlvryType == "delivery") {
                    cell.addHt.constant = 40
                    cell.addressLabel.text = "\(dict["address"] as! String), \(dict["city"] as! String), \((dict["pincode"] as! String).uppercased())"
                }
                else {
                    cell.addHt.constant = 0
                }
                cell.deliveryTime.text = "\(dict["delivery_time"] as! String)"
                
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (tableStr == "mm") {
           
            if (indexPath.row < 10) {
                 monthButton.setTitle("0\(indexPath.row + 1)", for: .normal)
            }
            else {
                 monthButton.setTitle("\(indexPath.row + 1)", for: .normal)
            }
        }
        else {
           yearButton.setTitle(yearArray[indexPath.row], for: .normal)
        }
        expiryTable.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (tableStr == "mm") || (tableStr == "yy") {
            return 30
        }
        else {
            if (indexPath.section == 0) {
                return 50
            }
            else if (indexPath.section == 1) {
                return 50
            }
            else {
                return 219
            }
        }
    }
}

class Order1TableViewCell: UITableViewCell {
    // order1cell
    @IBOutlet var itemName: UILabel!
    @IBOutlet var itemPrice: UILabel!
}

class Order2TableViewCell: UITableViewCell {
 // order2cell
    
    @IBOutlet var subTotal: UILabel!
    @IBOutlet var deliveryFee: UILabel!
    @IBOutlet var totalPrice: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var deliveryTime: UILabel!
    @IBOutlet var addHt: NSLayoutConstraint!
    
}
