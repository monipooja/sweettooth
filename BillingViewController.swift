//
//  BillingViewController.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 4/13/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import SystemConfiguration

class BillingViewController: UIViewController {
    
    @IBOutlet var addressView: UIView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var addressLabel: UILabel!
    
    @IBOutlet var addressHtConstraint: NSLayoutConstraint!
    @IBOutlet var addVwHtConstraint: NSLayoutConstraint!
    
    @IBOutlet var okButton: UIButton!
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var orderno: UILabel!
    @IBOutlet var orderdate: UILabel!
    
    @IBOutlet var restAddress: UILabel!
    @IBOutlet var restContact: UILabel!
    @IBOutlet var billingTable: UITableView!
    @IBOutlet var deliverTime: UILabel!
    
    @IBOutlet var billingTableHt: NSLayoutConstraint!
    @IBOutlet var contentViewHt: NSLayoutConstraint!
    @IBOutlet var scrollVw: UIScrollView!
    
    @IBOutlet var adHt: NSLayoutConstraint!
    
    var passDict : NSDictionary = [:]
    var orderArray : [NSDictionary] = []
    var ht : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        okButton.layer.cornerRadius = 20.0
        addressView.layer.cornerRadius = 4.0
        addressView.layer.borderWidth = 1.0
        addressView.layer.borderColor = UIColor.white.cgColor
        
        titleLabel.text = "You have placed an order from \((passDict["restaurant_name"] as? String)!)"
        restAddress.text = "\(passDict["restaurant_address"] as! String), \(passDict["restaurant_city"] as! String), \(passDict["restaurant_pincode"] as! String)"
        
        let mb = passDict["restaurant_contact_number"] as? String
        if (mb == "")
        {
             restContact.text = "Contact number - Not available"
        }
        else {
             restContact.text = "Contact number - \((mb)!)"
        }
        
        let dateString1 = (passDict["updated_at"] as! String)
        let format = "yyyy-MM-dd HH:mm:ss"
        
        orderdate.text = "\(dateString1.toDateString(inputFormat: format, outputFormat: "dd/MM/yyyy,HH:mm:ss")!)"
        orderno.text = (passDict["order_id"] as! String)
        
        userName.text = passDict["name"] as? String
        
        let httt = ApiResponse.calculateHeightForString("\((passDict["address"] as? String)!), \((passDict["city"] as? String)!), \(((passDict["pincode"] as? String)?.uppercased())!)")
        
        let dlvryType = passDict["product_delivery_type"] as? String
        
        if (dlvryType == "Delivery") || (dlvryType == "delivery") {
            
            addressHtConstraint.constant = httt + 20
            addVwHtConstraint.constant = 138 + httt
            
            addressLabel.text = "\((passDict["address"] as? String)!), \((passDict["city"] as? String)!), \(((passDict["pincode"] as? String)?.uppercased())!)"
            addressLabel.numberOfLines = 0
        }
        else {
            addressHtConstraint.constant = 0
            addVwHtConstraint.constant = 138
        }
        
        deliverTime.text = passDict["delivery_time"] as? String
        
        billingTable.separatorStyle = UITableViewCellSeparatorStyle.none
        billingTable.layer.borderWidth = 1.0
        billingTable.layer.borderColor = UIColor.white.cgColor
        
        self.setScrollView()
    }
    
    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  func setScrollView()
    {
        if (orderArray.count  != 0)
        {
            if (orderArray.count == 1) {
                ht = ((orderArray.count) * 50) + 116
            }
            else {
                ht = ((orderArray.count - 1) * 50) + 166
            }
            billingTableHt.constant = CGFloat(ht)
            
            scrollVw.isScrollEnabled = true
            contentViewHt.constant = 254 + billingTableHt.constant + 250
        }
    }
    
    @IBAction func onOk(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.add111()
    }

}

class Billing1TableViewCell: UITableViewCell {
    @IBOutlet var productName: UILabel!
    @IBOutlet var productPrice: UILabel!
}

class Billing2TableViewCell: UITableViewCell {
    @IBOutlet var subtotal: UILabel!
    @IBOutlet var deliveryFee: UILabel!
    @IBOutlet var totalPrc: UILabel!
}

extension BillingViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return orderArray.count
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section == 0) {
            let cell : Billing1TableViewCell = billingTable.dequeueReusableCell(withIdentifier: "bill1cell") as! Billing1TableViewCell
            
            let dict = orderArray[indexPath.row]
            cell.productName.text = "\(dict["product_name"] as! String) (\(dict["quantity"] as! String))"
            cell.productPrice.text = "\u{00A3}\(dict["price"] as! String)"
            
            return cell
        }
        else {
            let cell : Billing2TableViewCell = billingTable.dequeueReusableCell(withIdentifier: "bill2cell") as! Billing2TableViewCell
            
            let twoDecimalPlaces1 = String(format: "%.2f", Float((passDict["total_amount"] as? NSNumber)!))
            cell.subtotal.text = "\u{00A3}\(twoDecimalPlaces1)"
            let twoDecimalPlaces2 = String(format: "%.2f", Float((passDict["delivery_fee"] as? String)!)!)
            cell.deliveryFee.text = "\u{00A3}\(twoDecimalPlaces2)"
            let tot = Float((passDict["total_amount"] as? NSNumber)!) + Float((passDict["delivery_fee"] as? String)!)!
            let twoDecimalPlaces3 = String(format: "%.2f", tot)
            cell.totalPrc.text = "\u{00A3}\(twoDecimalPlaces3)"
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.section == 0) {
            return 50
        }
        else {
            return 116
        }
    }
}
