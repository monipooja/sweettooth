//
//  NewHistoryViewController.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 4/19/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import SystemConfiguration

class NewHistoryViewController: UIViewController {

    @IBOutlet var tableHistory: UITableView!
    
    var userid : String = ""
    var dictArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userid = (user.object(forKey: "logindata") as! NSDictionary)["user_id"] as! String
        self.tableHistory.isHidden = true
        
        DispatchQueue.main.async(execute: { () -> Void in
            LoadingIndicatorView.show()
            self.GetHistory()
        })
    }
    
    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBack(_ sender: Any)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.add111()
    }
    
    func GetHistory()
    {
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let params = "user_id=\(userid)"
            ApiResponse.onResponsePostPhp(url: "home/sthistory", parms: params, completion: { (result, error) in
                
                if (error == "") {
                    
                    print("history111 result = \(result)")
                    
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    if (status == true)
                    {
                        OperationQueue.main.addOperation {
                            self.dictArray = result["data"] as! [NSDictionary]
                            if (self.dictArray.count != 0)
                            {
                                self.tableHistory.isHidden = false
                                self.tableHistory.reloadData()
                            }
                            else {
                                self.tableHistory.isHidden = true
                            }
                            LoadingIndicatorView.hide()
                        }
                    }
                    else {
                        ApiResponse.alert(title: "Ooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
            ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }
}

class historytableViewCell:UITableViewCell
{
    
    @IBOutlet var restImage: UIImageView!
    @IBOutlet var restaurentName: UILabel!
    @IBOutlet var orderDate: UILabel!
    @IBOutlet var totalPrice: UILabel!
    @IBOutlet var deliveryType: UILabel!
    @IBOutlet var viewDetails: UIButton!
    
    @IBOutlet var nameHtConstraint: NSLayoutConstraint!
    
}

extension NewHistoryViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dictArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : historytableViewCell = tableHistory.dequeueReusableCell(withIdentifier: "NHistory")! as! historytableViewCell
        
        let dict = dictArray[indexPath.section]
        
        cell.nameHtConstraint.constant = ApiResponse.calculateHeightForString((dict["restaurant_name"] as? String)!)
        cell.restaurentName.text = dict["restaurant_name"] as? String
        cell.restaurentName.numberOfLines = 0
        
        let dtype = dict["product_delivery_type"] as? String
        if (dtype == "Delivery") || (dtype == "delivery") {
            cell.deliveryType.text = "Delivered"
        }
        else {
            cell.deliveryType.text = "Collected"
        }
        
        cell.restImage.sd_setImage(with: NSURL(string : "\(Constant.BASE_URL)\((dict["image"] as? String)!)") as URL!)
        
        let twoDecimalPlaces1 = String(format: "%.2f", Float((dict["total_price"] as! String))!)
        let qty = dict["total_quantity"] as! String
        if (qty == "1") {
             cell.totalPrice.text = "\u{00A3}\(twoDecimalPlaces1) (\(dict["total_quantity"] as! String) Item)"
        }
        else {
             cell.totalPrice.text = "\u{00A3}\(twoDecimalPlaces1) (\(dict["total_quantity"] as! String) Items)"
        }
        
        let dateString1 = (dict["time"] as! String)
        let format = "yyyy-MM-dd HH:mm:ss"
        
        cell.orderDate.text = "\(dateString1.toDateString(inputFormat: format, outputFormat: "dd/MM/yyyy, HH:mm:ss")!)"

        cell.viewDetails.addTarget(self, action: #selector(self.ShowDetail(_:)), for: .touchUpInside)

        return cell
    }
    
    func ShowDetail(_ sender : UIButton) {
        
        let point : CGPoint = sender.convert(CGPoint.zero, to: tableHistory)
        let indexPath1 = tableHistory.indexPathForRow(at: point)
        
        let dict = dictArray[(indexPath1?.section)!]
        
        let orderIdPass = dict["odear_id"] as! String
        
        let orderD = self.storyboard?.instantiateViewController(withIdentifier: "orderDetails1") as! OrderDetailsViewController
        
        orderD.orderId1 = orderIdPass
        self.present(orderD, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView.init(frame: CGRect(x : 0, y : 0, width : tableView.frame.width, height : 1))
        footerView.backgroundColor = UIColor.lightGray
        
        return footerView
    }
}



