//
//  DashboardViewController.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 3/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift
import MapKit
import SystemConfiguration

class DashboardViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, CLLocationManagerDelegate, MKMapViewDelegate {

    static let identifier = String("dashboard")
    
    @IBOutlet var pinCode: UITextField!
    @IBOutlet var cartItems: UILabel!
    
    @IBOutlet var searchButton: UIButton!
    @IBOutlet var locationButton: UIButton!
    
    @IBOutlet var ratingView: UIView!
    @IBOutlet var restImage: UIImageView!
    @IBOutlet var restName: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var postcodeLabel: UILabel!
    @IBOutlet var contactNumber: UILabel!
    @IBOutlet var commentTextView: UITextView!
    @IBOutlet var subButtion: UIButton!
    
    @IBOutlet var starCollection: [UIButton]!
    
    var lat1 : CLLocationDegrees!
    var lng1 : CLLocationDegrees!
    var Postcode : String = ""
    var newFrame : CGRect!
    var rating123 = 0, rating = 0
    var userid : String = ""
    var restId : String = ""
    
    var newLocation: CLLocation!
    var locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        SlideMenuOptions.leftViewWidth = self.view.bounds.width - 30
        SlideMenuOptions.contentViewDrag = false
        SlideMenuOptions.hideStatusBar = false
        SlideMenuOptions.contentViewScale = 1.0

        pinCode.layer.cornerRadius = 20
        pinCode.layer.borderWidth = 1.0
        pinCode.layer.borderColor = UIColor.white.cgColor
        pinCode.textAlignment = NSTextAlignment.center
        
        searchButton.layer.cornerRadius = 20
        locationButton.layer.cornerRadius = 20
        
        pinCode.text = ""
        pinCode.attributedPlaceholder = NSAttributedString(string:"Enter your postcode",
                                                      attributes:[NSForegroundColorAttributeName: UIColor.white])
        pinCode.delegate = self
        
        //// *** RATING WORK *** ////
        commentTextView.layer.cornerRadius = 4.0
        commentTextView.text = "Please provide a comment...."
        commentTextView.layer.borderColor = UIColor.lightGray.cgColor
        commentTextView.layer.borderWidth = 0.8
        
        commentTextView.textColor = UIColor.lightGray
        commentTextView.delegate = self
        
        ratingView.layer.cornerRadius = 4.0
        
        for bb in self.starCollection as [UIButton]
        {
            bb.isHidden = false
            // bb.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "buttonClicked:"))
            bb.setBackgroundImage(UIImage(named : "starGray.png"), for: UIControlState())
            
            bb.addTarget(self, action: #selector(DashboardViewController.buttonClicked(_:)), for: .touchUpInside)
        }
        
        userid = (user.object(forKey: "logindata") as! NSDictionary)["user_id"] as! String
        
        self.currentLocationOfUser()
    }
    
    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.async(execute: { () -> Void in
            
            let parameters = "user_id=\(self.userid)"
            self.callService("home/addcartcount", params: parameters, check: "count")
        })
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText : NSString = textView.text as NSString
        let updatedText = currentText.replacingCharacters(in: range, with: text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {
            
            textView.text = "Please provide a comment...."
            textView.textColor = UIColor.lightGray
            
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            
            return false
        }
            
        else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
        if(text == "\n") {
            textView.resignFirstResponder()
//            textView.text = "Please provide a comment...."
//            textView.textColor = UIColor.lightGray
            
//            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            return false
        }
        
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        self.newFrame = self.view.frame
        
        newFrame.origin.y = self.view.bounds.origin.y - 150
        newFrame.size.height = self.view.bounds.size.height
        
        UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                
                self.view.frame = self.newFrame
        })
    }
    
    func textViewDidEndEditing(_ textView: UITextView)
    {
        newFrame.origin.y = 0
        newFrame.size.height = self.view.bounds.size.height
        
        UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                self.view.frame = self.newFrame
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
         if DeviceType.IS_IPHONE_5
         {
            self.newFrame = self.view.frame
            
            newFrame.origin.y = self.view.bounds.origin.y - 110
            newFrame.size.height = self.view.bounds.size.height
            
            UIView.animate(withDuration: 0.25, animations:
            {() -> Void in
                    
                self.view.frame = self.newFrame
            })
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        if DeviceType.IS_IPHONE_5
        {
            newFrame.origin.y = 0
            newFrame.size.height = self.view.bounds.size.height
            
            UIView.animate(withDuration: 0.25, animations:
                {() -> Void in
                    self.view.frame = self.newFrame
            })
        }
    }
    
    func currentLocationOfUser()
    {
        //print("1")
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        // A minimum distance a device must move before update event generated
        locationManager.distanceFilter = 500
        // Request permission to use location service
        locationManager.requestWhenInUseAuthorization()
        // Request permission to use location service when the app is run
        // locationManager.requestAlwaysAuthorization()
        // Start the update of user's location
        locationManager.startUpdatingLocation()
    }
    
//    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
//    {
//        if (status == CLAuthorizationStatus.authorizedWhenInUse)
//        {
//            locationManager.startUpdatingLocation()
//        }
//    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        //print("3")
        newLocation = locations.last
        //  print("method 3 = \(newLocation?.coordinate)")
        
        lat1 = newLocation!.coordinate.latitude
        lng1 = newLocation!.coordinate.longitude

        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)-> Void in
            if (error != nil) {
                print("Reverse geocoder failed with error" + (error?.localizedDescription)!)
                return
            }
            
            if placemarks?.count != 0 {
                let pm = (placemarks?[0])! as CLPlacemark
                self.Postcode = pm.postalCode!
                
                print("lat1 = \(self.lat1) ***** lng1 = \(self.lng1)******\(self.Postcode)")
            } else {
                print("Problem with the data received from geocoder")
            }
        })
    }
    
    @IBAction func searchByLocation(_ sender: Any) {
        
        if (lat1 != nil) && (lng1 != nil) {
            LoadingIndicatorView.show()
            let parameters = "latitude=\(lat1!)&longitude=\(lng1!)"
            self.callService("restaurant/restaurant", params: parameters, check: "location")
        }
    }
    
    @IBAction func onSearch(_ sender: Any) {
        
        if (pinCode.text == "") {
            ApiResponse.alert(title: "Blank", message: "Please enter postcode", controller: self)
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
           // pinCode.text = pinCode.text?.uppercased()
            LoadingIndicatorView.show()
            let parameters = "pincode=\(pinCode.text!)"
            self.callService("restaurant/restaurant", params: parameters, check: "search")
        }
        else {
            ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }
    
    func callService(_ urlStr : String, params : String, check : String)
    {
        ApiResponse.onResponsePostPhp(url: urlStr, parms: params, completion: { (result, error) in
            
            if (error == "") {
                
                print("search result = \(result)")
                let status = result["status"] as! Bool
                let message = result["message"] as! String
                
                if (status == true) {
                    OperationQueue.main.addOperation {
                        
                        LoadingIndicatorView.hide()
                        if (check == "search") || (check == "location") {
                            let data = result["data"] as! NSDictionary
                            let arrayDict = data["restaurant"] as! [NSDictionary]
                            let search = self.storyboard?.instantiateViewController(withIdentifier: "search") as! DashboardSearchVC
                            search.pasArray = arrayDict
                            if (check == "search") {
                               search.pincode = self.pinCode.text!
                            }
                            else {
                               search.pincode = self.Postcode
                            }
                            self.present(search, animated: true, completion: nil)
                        }
                        else if (check == "submit") {
                            self.ratingView.isHidden = true
                            ApiResponse.alert(title: "", message: message, controller: self)
                        }
                        else if (check == "cancel") {
                            self.ratingView.isHidden = true
                        }
                        else {
                            self.cartItems.text = "\((result["data"] as? NSNumber)!)"
                            user.set(self.cartItems.text!, forKey: "itemcount")
                            user.synchronize()
                            
                            let ratingCheck = result["status_rating"] as! Bool
                            if (ratingCheck == true) {
                                self.ratingView.isHidden = false
                                self.setRestuarantDetails(dict: result["restaurant_details"] as! NSDictionary)
                            }
                            else {
                                self.ratingView.isHidden = true
                            }
                        }
                    }
                }
                else {
                    OperationQueue.main.addOperation {
                        if (check == "count") {
                            self.cartItems.text = "0"
                            user.set("0", forKey: "itemcount")
                            user.synchronize()
                            
                            let ratingCheck = result["status_rating"] as! Bool
                            if (ratingCheck == true) {
                                self.ratingView.isHidden = false
                                self.setRestuarantDetails(dict: result["restaurant_details"] as! NSDictionary)
                            }
                            else {
                                self.ratingView.isHidden = true
                            }
                        }
                        else if (check == "cancel") {
                            self.ratingView.isHidden = true
                        }
                        else {
                            ApiResponse.alert(title: "Oooops", message: message, controller: self)
                        }
                    }
                }
            }
            else {
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
    }
    
    func setRestuarantDetails( dict : NSDictionary)
    {
        restImage.sd_setImage(with: NSURL(string : "\(Constant.BASE_URL)\(dict["image"] as! String)") as URL!)
        restName.text = dict["restaurant_name"] as? String
        addressLabel.text = dict["address"] as? String
        postcodeLabel.text = "Postcode : \(dict["pincode"] as! String)"
        postcodeLabel.text = "Contact number : \(dict["contact_number"] as! String)"
        
        restId = (dict["id"] as? String)!
    }

    @IBAction func openCart(_ sender: Any) {
        
        if (self.cartItems.text == "") || (self.cartItems.text == "0") {
            ApiResponse.alert(title: "Oooops", message: "Cart is empty", controller: self)
        }
        else {
            let cart = self.storyboard?.instantiateViewController(withIdentifier: "cart") as! MyCartViewController
            self.present(cart, animated: true, completion: nil)
        }
    }
    
    func buttonClicked(_ button: UIButton)
    {
        print("Button pressed 👍")
        
        for bb in self.starCollection as [UIButton]
        {
            bb.setBackgroundImage(UIImage(named : "starGray.png"), for: UIControlState())
            //bb.hidden = true
        }
        
        for i in 0  ... (starCollection.index(of: button)!)
        {
            starCollection[i].setBackgroundImage(UIImage (named: "starPink.png"), for: UIControlState())
            
            rating = starCollection.index(of: button)!
            
            print("rating = \(rating +  1)")
        }
        rating123 = rating + 1
    }
    
    @IBAction func submitRating(_ sender: Any) {
        
        var comment : String = ""
        if (rating123 == 0) {
            ApiResponse.alert(title: "", message: "Please provide rating", controller: self)
        }
        else {
            if commentTextView.textColor == UIColor.lightGray && !commentTextView.text.isEmpty {
                comment = ""
            }
            else {
                comment = commentTextView.text
            }
            LoadingIndicatorView.show()
            let parameters = "rating=\(rating123)&comment=\(comment)&user_id=\(self.userid)&restaurant_id=\(restId)&type=submit"
            self.callService("Home/rating", params: parameters, check: "submit")
        }
    }
    
    @IBAction func cancelRating(_ sender: Any) {
        
        rating123 = 0
        let parameters = "rating=\(rating123)&comment=&user_id=\(self.userid)&restaurant_id=\(restId)&type=cancel"
        self.callService("Home/rating", params: parameters, check: "cancel")
    }
    
    @IBAction func openSidebar(_ sender: Any) {
         slideMenuController()?.openLeft()
    }
    
}

extension DateFormatter {
    
    convenience init (format: String) {
        self.init()
        dateFormat = format
        locale = Locale.current
    }
}

extension String {
    
    func toDate (format: String) -> Date? {
        return DateFormatter(format: format).date(from: self)
    }
    
    func toDateString (inputFormat: String, outputFormat:String) -> String? {
        if let date = toDate(format: inputFormat) {
            return DateFormatter(format: outputFormat).string(from: date)
        }
        return nil
    }
}

enum UIUserInterfaceIdiom : Int
{
    case Unspecified
    case Phone
    case Pad
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}
