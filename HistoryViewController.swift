//
//  HistoryViewController.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 3/30/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {

    @IBOutlet var historyCollectionView: UICollectionView!
    
    var userid : String = ""
    var dictArray : [NSDictionary] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        userid = (user.object(forKey: "logindata") as! NSDictionary)["user_id"] as! String
        
        DispatchQueue.main.async(execute: { () -> Void in
            LoadingIndicatorView.show()
            self.GetHistory()
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func GetHistory()
    {
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN
        {
            let params = "user_id=\(userid)"
            ApiResponse.onResponsePostPhp(url: "sweettooth/home/sthistory", parms: params, completion: { (result, error) in
            
                if (error == "") {
                    
                    print("history result = \(result)")
                    
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    if (status == true)
                    {
                        OperationQueue.main.addOperation {
                            self.dictArray = result["data"] as! [NSDictionary]
                            if (self.dictArray.count != 0)
                            {
                               self.historyCollectionView.reloadData()
                            }
                            else {
                                self.historyCollectionView.isHidden = true
                            }
                            LoadingIndicatorView.hide()
                        }
                    }
                    else {
                        ApiResponse.alert(title: "Ooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
            ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }

    @IBAction func openDashboard(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.add111()
    }
}

class HistoryCollectionViewCell : UICollectionViewCell {
    // historycell
    
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var qtyLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var bookingDate: UILabel!
}

extension HistoryViewController : UICollectionViewDataSource, UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dictArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : HistoryCollectionViewCell = historyCollectionView.dequeueReusableCell(withReuseIdentifier: "historycell", for: indexPath) as! HistoryCollectionViewCell
        
        let dict = dictArray[indexPath.row]
        
        cell.nameLabel.text = dict["product_name"] as? String
        cell.qtyLabel.text = "Qty : \(dict["quantity"] as! String)"
        let prc = Float(dict["price"] as! String)! * Float(dict["quantity"] as! String)!
        cell.priceLabel.text = "\u{00A3}\(prc)"
        
        let dateString1 = (dict["created_at"] as! String)
        let format = "yyyy-MM-dd HH:mm:ss"
        
        cell.bookingDate.text = "Booked on : \(dateString1.toDateString(inputFormat: format, outputFormat: "EEE,dd-MM-yyyy,HH:mm:ss")!)"
        
        cell.backgroundImage?.sd_setImage(with: NSURL(string : "https://www.ilearnersindia.com/sweettooth/\(dict["image"] as! String)") as! URL)
        
        return cell
    }
}

extension DateFormatter {
    
    convenience init (format: String) {
        self.init()
        dateFormat = format
        locale = Locale.current
    }
}

extension String {
    
    func toDate (format: String) -> Date? {
        return DateFormatter(format: format).date(from: self)
    }
    
    func toDateString (inputFormat: String, outputFormat:String) -> String? {
        if let date = toDate(format: inputFormat) {
            return DateFormatter(format: outputFormat).string(from: date)
        }
        return nil
    }
}

