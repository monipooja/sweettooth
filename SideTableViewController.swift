

import UIKit
import ReachabilitySwift
import SDWebImage
import SystemConfiguration

class SideTableViewController : UITableViewController {
    
    var arr : [String] = []
    var imageArr : [String] = []
    var orderImageArr : [String] = []
    var arr2 : [String] = []
    var arr3 : [String] = []
    var ImageArr3 : [String] = []
    
    var reach : Reachability!
    static let identifier = String("hamburger")
    var userid : String = ""
    var sectionStr : String = ""

    @IBOutlet var tablevw: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reach = Reachability()!
        
        userid = (user.object(forKey: "logindata") as! NSDictionary)["user_id"] as! String
        
        orderImageArr = ["yourorder.png", "orderhistory.png"]
        arr2 = ["Your Order", "Order History"]
    
        let imageView = UIImageView(image: UIImage(named : "02.png"))
        tablevw.backgroundView = imageView
    }
    
    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.async(execute: {() -> Void in
            
            self.callService()
        })
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(section == 0)
        {
           return 1
        }
        else if (section == 1)
        {
            return arr.count
        }
        else if (section == 2)
        {
             return arr2.count
        }
        else if (section == 3)
        {
            return arr3.count
        }
        else {
            return 1
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! tableViewCell

            let datadict = user.object(forKey: "logindata") as! NSDictionary
            
            cell.label.text = "\((datadict["first_name"] as? String)!) \((datadict["last_name"] as? String)!)"
            cell.emailLabel.text = datadict["email"] as? String

            return cell
        }
        else if (indexPath.section == 1)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell12", for: indexPath)
            if (arr.count == 5) {
                if (indexPath.row == 4){
                     cell.textLabel?.text = "\(arr[indexPath.row])       (\((user.object(forKey: "itemcount"))!))"
                }
                else {
                   cell.textLabel?.text = arr[indexPath.row]
                }
            }
            else {
                cell.textLabel?.text = arr[indexPath.row]
            }
            cell.textLabel?.textColor = UIColor.white
            cell.textLabel?.font = UIFont(name: "Open Sans", size: 15)
            
            cell.imageView?.image = UIImage(named:imageArr[indexPath.row])
            
            let itemSize = CGSize(width : 20, height : 20);
            UIGraphicsBeginImageContextWithOptions(itemSize, false, 0.0)
            let imageRect = CGRect(x : 0.0, y : 0.0, width : itemSize.width, height : itemSize.height);
            cell.imageView?.image?.draw(in: imageRect)
            cell.imageView?.image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            
            return cell
        }
        if (sectionStr == "") {
            if (indexPath.section == 2) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell14", for: indexPath) as! Order4TableViewCell
                return cell
            }
            else if (indexPath.section == 3)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell15", for: indexPath)
                if (arr3.count == 2) {
                    if (indexPath.row == 1){
                        cell.textLabel?.text = "\(arr3[indexPath.row])       (\((user.object(forKey: "itemcount"))!))"
                    }
                    else {
                        cell.textLabel?.text = arr3[indexPath.row]
                    }
                }
                else {
                    cell.textLabel?.text = arr3[indexPath.row]
                }
                cell.textLabel?.textColor = UIColor.white
                cell.textLabel?.font = UIFont(name: "Open Sans", size: 15)
                
                cell.imageView?.image = UIImage(named:ImageArr3[indexPath.row])
                
                let itemSize = CGSize(width : 20, height : 20);
                UIGraphicsBeginImageContextWithOptions(itemSize, false, 0.0)
                let imageRect = CGRect(x : 0.0, y : 0.0, width : itemSize.width, height : itemSize.height);
                cell.imageView?.image?.draw(in: imageRect)
                cell.imageView?.image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell13", for: indexPath)
                
                cell.textLabel?.text = "Log out"
                cell.textLabel?.textColor = UIColor.white
                cell.textLabel?.font = UIFont(name: "Open Sans", size: 15)
                
                cell.imageView?.image = UIImage(named: "28.png")
                
                let itemSize = CGSize(width : 20, height : 20);
                UIGraphicsBeginImageContextWithOptions(itemSize, false, 0.0)
                let imageRect = CGRect(x : 0.0, y : 0.0, width : itemSize.width, height : itemSize.height);
                cell.imageView?.image?.draw(in: imageRect)
                cell.imageView?.image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                return cell
            }
        }
        else {
            if (indexPath.section == 2) {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell14", for: indexPath) as! Order4TableViewCell
                cell.imageVw?.image = UIImage(named:orderImageArr[indexPath.row])
                cell.nameLabel.text = arr2[indexPath.row]
                    
                return cell
            }
            else if (indexPath.section == 3)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell15", for: indexPath)
                if (arr3.count == 2) {
                    if (indexPath.row == 1){
                        cell.textLabel?.text = "\(arr3[indexPath.row])       (\((user.object(forKey: "itemcount"))!))"
                    }
                    else {
                        cell.textLabel?.text = arr3[indexPath.row]
                    }
                }
                else {
                    cell.textLabel?.text = arr3[indexPath.row]
                }
                cell.textLabel?.textColor = UIColor.white
                cell.textLabel?.font = UIFont(name: "Open Sans", size: 15)
                
                cell.imageView?.image = UIImage(named:ImageArr3[indexPath.row])
                
                let itemSize = CGSize(width : 20, height : 20);
                UIGraphicsBeginImageContextWithOptions(itemSize, false, 0.0)
                let imageRect = CGRect(x : 0.0, y : 0.0, width : itemSize.width, height : itemSize.height);
                cell.imageView?.image?.draw(in: imageRect)
                cell.imageView?.image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell13", for: indexPath) 
                
                cell.textLabel?.text = "Log out"
                cell.textLabel?.textColor = UIColor.white
                cell.textLabel?.font = UIFont(name: "Open Sans", size: 15)
                
                cell.imageView?.image = UIImage(named: "28.png")
                
                let itemSize = CGSize(width : 20, height : 20);
                UIGraphicsBeginImageContextWithOptions(itemSize, false, 0.0)
                let imageRect = CGRect(x : 0.0, y : 0.0, width : itemSize.width, height : itemSize.height);
                cell.imageView?.image?.draw(in: imageRect)
                cell.imageView?.image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
                
                return cell
            }
        }
    }
        
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        if (indexPath.section == 1)
        {
            switch indexPath.row
            {

            case 0:
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.add111()
                break
                
            case 1:
                let profile = self.storyboard?.instantiateViewController(withIdentifier: "profile") as! ProfileViewController
                self.present(profile, animated: true, completion: nil)
                break
                
            case 2:
                print("order")
                if (sectionStr == "") {
                sectionStr = "section4"
                tableView.reloadData()
                }
                else {
                    sectionStr = ""
                    tableView.reloadData()
                }
                break
                
            default:
                print("default")
            }
        }
        else if (indexPath.section == 2)
        {
            //sectionStr = ""
            tableView.reloadData()
            switch indexPath.row
            {
            case 0:
                let yourorder = self.storyboard?.instantiateViewController(withIdentifier: "yourorder") as! YourOrderViewController
                self.present(yourorder, animated: true, completion: nil)
                break
                
            case 1:
                let history = self.storyboard?.instantiateViewController(withIdentifier: "history1") as! NewHistoryViewController
                self.present(history, animated: true, completion: nil)
                break
                
            default:
                print("default")
            }
        }
        else if (indexPath.section == 3)
        {
            switch indexPath.row
            {
            case 0:
                print("help")
                let cont = self.storyboard?.instantiateViewController(withIdentifier: "contact") as! ContactusViewController
                self.present(cont, animated: true, completion: nil)
                break
                
            case 1:
                print("order")
                let cart = self.storyboard?.instantiateViewController(withIdentifier: "cart") as! MyCartViewController
                self.present(cart, animated: true, completion: nil)
                break
                
            default:
                print("default")
            }
        }
        else {
            let alert = UIAlertController(title: "Logout", message: "Are you sure, you want to logout", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {
                action in
                let user = UserDefaults.standard
                user.set(false, forKey: "logout")
                user.synchronize()
                let his = self.storyboard?.instantiateViewController(withIdentifier: "view") as! ViewController
                self.present(his, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.destructive, handler:nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(indexPath.section == 0)
        {
            return 175
        }
        else if (indexPath.section == 1)
        {
            return 45
        }
        if (sectionStr == "") {
          if (indexPath.section == 2) {
            
                return 0
          }
          else if (indexPath.section == 3) {
                
                return 45
          }
          else {
                return 40
          }
        }
        else {
            if (indexPath.section == 2) {
                
                return 45
            }
            else if (indexPath.section == 3) {
                
                return 45
            }
            else {
                return 40
            }
        }
    }
    
    func callService()
    {
        let params = "user_id=\(userid)"
        
        ApiResponse.onResponsePostPhp(url: "home/addcartcount", parms: params, completion: { (result, error) in
            
            if (error == "") {
                
                print("search result = \(result)")
                let status = result["status"] as! Bool
                if (status == true) {
                    OperationQueue.main.addOperation {

                        LoadingIndicatorView.hide()
                        let orderCount = "\((result["data"] as? NSNumber)!)"
                         print("orderrrrrrr = \(orderCount)")
                        if (orderCount == "0") {
                            self.arr = ["Search", "Profile", "Order"]
                            self.imageArr = ["search-1.png", "23.png", "25.png"]
                            
                            self.arr3 = ["Help"]
                            self.ImageArr3 = ["26.png"]
                        }
                        else {
                            self.arr = ["Search", "Profile", "Order"]
                            self.imageArr = ["search-1.png", "23.png", "25.png"]
                            
                            self.arr3 = ["Help", "Current Order"]
                            self.ImageArr3 = ["26.png", "19.png"]
                        }
                        self.tablevw.reloadData()
                        user.set(orderCount, forKey: "itemcount")
                        user.synchronize()
                    }
                }
                else {
                    OperationQueue.main.addOperation {
                        self.arr = ["Search", "Profile", "Order"]
                        self.imageArr = ["search-1.png", "23.png", "25.png"]
                        
                        self.arr3 = ["Help"]
                        self.ImageArr3 = ["26.png"]
                        
                        self.tablevw.reloadData()
                        user.set("0", forKey: "itemcount")
                        user.synchronize()
                    }
                }
            }
            else {
                if (error == Constant.Status_Not_200) {
                    ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                }
                else {
                    ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                }
            }
        })
    }
    
    func callAlert(_ title: String , msg : String)
    {
        OperationQueue.main.addOperation
        {
            let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

class tableViewCell : UITableViewCell
{
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet weak var label: UILabel!
}

class Order4TableViewCell : UITableViewCell {
    
    @IBOutlet var imageVw: UIImageView!
    @IBOutlet var nameLabel: UILabel!
}

//class LogoutTableViewCell: UITableViewCell {
//    
//    @IBOutlet var logoutButton: UIButton!
//}
