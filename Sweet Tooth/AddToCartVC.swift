//
//  AddToCartVC.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 6/22/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import SystemConfiguration

class AddToCartVC: UIViewController, UITextViewDelegate {

    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var cartButton: UIButton!
    @IBOutlet var titleName: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var noteTextview: UITextView!
    
    @IBOutlet var addToCartButton: UIButton!
    @IBOutlet var quantityLabel: UILabel!
    @IBOutlet var qtyLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var plusMinusViiew: UIView!
    
    var passDict : NSDictionary = [:]
    var finalQuantity : Int = 1
    var finalPrice : Float = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addToCartButton.layer.cornerRadius = 4.0
        
        cartButton.layer.masksToBounds = false
        cartButton.layer.cornerRadius = cartButton.frame.size.width/2.0
        cartButton.clipsToBounds = true
        cartButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
        let items = user.object(forKey: "itemcount") as! String
        cartButton.setTitle(items, for: .normal)
        
        plusMinusViiew.layer.cornerRadius = 25
        plusMinusViiew.layer.borderColor = UIColor.lightGray.cgColor
        plusMinusViiew.layer.borderWidth = 0.8
        
        titleName.text = passDict["sweets_name"] as? String
        descriptionLabel.text = passDict["dessert_description"] as? String
        let twoDecimalPlaces = String(format: "%.2f", Float((passDict["price"] as? String)!)!)
        priceLabel.text = "\u{00A3}\(twoDecimalPlaces)"
        backgroundImage.sd_setImage(with: NSURL(string : "\(Constant.BASE_URL)\(passDict["sweets_image"] as! String)") as URL!)
        
        noteTextview.text = "Add a note(extra sauce,extra toppings,etc.)"
        noteTextview.textColor = UIColor.lightGray
        noteTextview.delegate = self
    }

    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText : NSString = textView.text as NSString
        let updatedText = currentText.replacingCharacters(in: range, with: text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {
            
            textView.text = "Add a note(extra sauce,extra toppings,etc.)"
            textView.textColor = UIColor.lightGray
            
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            
            return false
        }
            
        else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        
        if(text == "\n") {
            textView.resignFirstResponder()
            textView.text = "Add a note(extra sauce,extra toppings,etc.)"
            textView.textColor = UIColor.lightGray
            
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            return false
        }
        
        return true
    }
    
    func GetCategoryData(_ urlStr : String, Parameters : String, check : String)
    {
        LoadingIndicatorView.show()
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN {
        
            ApiResponse.onResponsePostPhp(url: urlStr, parms: Parameters, completion: { (result, error) in
                
                if (error == "") {
                    print("add to cart result = \(result) \(check)")
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    if (status == true) {
                        OperationQueue.main.addOperation {
                            let data = result["data"] as! NSDictionary
                            print("ccccccc = \((data["count"] as? NSNumber)!)")
                            self.cartButton.setTitle("\((data["count"] as? NSNumber)!)", for: .normal)
                            user.set((data["count"] as? NSNumber), forKey: "itemcount")
                            user.synchronize()
                            ApiResponse.alert(title: "Done", message: message, controller: self)
                        }
                    }
                    else {
                        ApiResponse.alert(title: "Ooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
            ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }
    
    @IBAction func increaseQuantity(_ sender: Any) {
        
        finalQuantity = finalQuantity + 1
        quantityLabel.text = "\(finalQuantity)"
        qtyLabel.text = "ADD \(finalQuantity) TO CART"
        
        let prc = Float(finalQuantity) * Float((passDict["price"] as? String)!)!
        let twoDecimalPlaces = String(format: "%.2f", prc)
        priceLabel.text = "\u{00A3}\(twoDecimalPlaces)"
    
    }

    @IBAction func decreaseQuantity(_ sender: Any) {
        
        if (finalQuantity > 1) {
            finalQuantity = finalQuantity - 1
            quantityLabel.text = "\(finalQuantity)"
            qtyLabel.text = "ADD \(finalQuantity) TO CART"
            
            let prc = Float(finalQuantity) * Float((passDict["price"] as? String)!)!
            let twoDecimalPlaces = String(format: "%.2f", prc)
            priceLabel.text = "\u{00A3}\(twoDecimalPlaces)"
        }
    }
    
    @IBAction func goToCart(_ sender: Any) {
        
        if (self.cartButton.titleLabel?.text == "") || (self.cartButton.titleLabel?.text == "0") {
            ApiResponse.alert(title: "Oooops", message: "Cart is empty", controller: self)
        }
        else {
            let cart = self.storyboard?.instantiateViewController(withIdentifier: "cart") as! MyCartViewController
            self.present(cart, animated: true, completion: nil)
        }
    }
    
    @IBAction func addToCart(_ sender: Any) {
        
        var noteText = ""
        if noteTextview.textColor == UIColor.lightGray && noteTextview.text.isEmpty {
            noteText = ""
        }
        else {
            noteText = noteTextview.text
        }
        let userid = (user.object(forKey: "logindata") as! NSDictionary)["user_id"] as! String
        let params = "user_id=\(userid)&product_id=\(passDict["id"] as! String)&restaurant_id=\(passDict["restaurant_id"] as! String)&product_note=\(noteText)&quantity=\(finalQuantity)"
        self.GetCategoryData("cart/add_cart", Parameters: params, check: "add")
    }
    
    @IBAction func onGoBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }

}
