



import UIKit
import SystemConfiguration

class CheckoutViewController: UIViewController {
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var orderListTable: UITableView!
    @IBOutlet var contentView: UIView!
    @IBOutlet var orderButton: UIButton!
    
    @IBOutlet var tableHtConstraint: NSLayoutConstraint!
    @IBOutlet var contentHtConstraint: NSLayoutConstraint!
    
    @IBOutlet var restaurentNAme: UILabel!
    @IBOutlet var restaurentAddress: UILabel!
    
    var totalQuantity : Int = 0
    var totalPrice : Float = 0.00
    var actualPrc : [String] = []
    var productArray : [NSDictionary] = []
    var userid : String = ""
    var quantArray : [Int] = []
    var jsonArray : [NSDictionary] = []
    var quantdict : [String : AnyObject] = [:]
    var ht : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        orderListTable.layer.cornerRadius = 5.0
        orderButton.layer.cornerRadius = 20.0
        
        self.orderListTable.isHidden = true
        self.orderButton.isHidden = true
        
        userid = (user.object(forKey: "logindata") as! NSDictionary)["user_id"] as! String
        
        DispatchQueue.main.async(execute: { () -> Void in
            
            LoadingIndicatorView.show()
            let parameters = "user_id=\(self.userid)"
            self.GetCartList("home/show_cartdetails", params: parameters, check: "list")
        })
    }
    
    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func GetCartList(_ urlstr : String, params : String, check : String)
    {
        if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN {
            
            ApiResponse.onResponsePostPhp(url: urlstr, parms: params, completion: { (result, error) in
                
                if (error == "") {
                    print("update cart result = \(result)")
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    if (status == true) {
                        OperationQueue.main.addOperation {
                            
                            LoadingIndicatorView.hide()
                            if (check == "list") {
                                self.productArray = result["data"] as! [NSDictionary]
                                
                                user.set("\(self.productArray.count)", forKey: "itemcount")
                                user.synchronize()
                                
                                for prod in self.productArray {
                                    
                                    self.restaurentNAme.text = prod["restaurant_name"] as? String
                                    self.restaurentAddress.text = "\((prod["address"] as? String)!), \((prod["city"] as? String)!), \(((prod["pincode"] as? String)?.uppercased())!)\n Contact number - \(prod["contact_number"] as! String)"
                                    
                                    let prc = prod["price"] as? String
                                    let qty = prod["quantity"] as? String
                                    self.totalPrice =  self.totalPrice + (Float(prc!)! * Float(qty!)!)
                                    self.quantArray.append(Int((prod["quantity"] as? String)!)!)
                                }
                                DispatchQueue.main.async(execute: { () -> Void in
                                    self.orderListTable.isHidden = false
                                    self.orderButton.isHidden = false
                                    self.orderListTable.reloadData()
                                    self.setScrollView()
                                })
                            }
                            else if (check == "remove") {
                                
                                self.productArray = result["data"] as! [NSDictionary]
                                
                                user.set("\(self.productArray.count)", forKey: "itemcount")
                                user.synchronize()
                                self.totalPrice = 0.00
                                self.viewDidLoad()
                                ApiResponse.alert(title: "Done", message: "Product removed successfully", controller: self)
                            }
                            else {
                                back = "appear"
                                ApiResponse.alert(title: "Done", message: message, controller: self)
                                self.dismiss(animated: true, completion: nil)
                            }
                        }
                    }
                    else {
                        ApiResponse.alert(title: "Ooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
            ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }
    
    func setScrollView()
    {
        if (productArray.count  != 0)
        {
            if (productArray.count == 1) {
                ht = ((productArray.count) * 55)
            }
            else {
                ht = ((productArray.count - 1) * 55)
            }
            
            tableHtConstraint.constant = (orderListTable.bounds.size.height)+CGFloat(ht)
            
//            if ((orderButton.frame.origin.y + orderButton.bounds.size.height + CGFloat(ht) + 60) > 508)
//            {
                scrollView.isScrollEnabled = true
                contentHtConstraint.constant = orderButton.frame.origin.y + orderButton.bounds.size.height +  CGFloat(ht) + 90
//            }
//            else
//            {
//                scrollView.isScrollEnabled = false
//                contentHtConstraint.constant = 610
//            }
        }
        else
        {
            orderListTable.isHidden = true
            orderButton.isHidden = true
            
            let lab =  UILabel.init()
            lab.frame.size = CGSize(width : 200, height : 50)
            lab.center = self.view.center
            lab.text = "Your cart is empty"
            lab.textAlignment = NSTextAlignment.center
            lab.font = UIFont(name: "Open Sans", size: 18)
            self.view.addSubview(lab)
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        back = "appear"
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func placeOrder(_ sender: Any)
    {
        var jsonArray : [NSDictionary] = []
        var i = 0
        for dict in productArray
        {
            
            let prc = dict["price"] as? String
            let qty = quantArray[i]
            
            let dict1 = ["id": dict["id"] as AnyObject , "user_id": dict["user_id"] as AnyObject, "quantity": "\(quantArray[i] as AnyObject)", "product_id": dict["product_id"] as AnyObject, "price": "\((Int(prc!)! * qty))", "product_name" : dict["product_name"] as AnyObject, "restaurant_id" : dict["restaurant_id"] as AnyObject, "product_note" : dict["product_note"] as AnyObject] as [String : Any]
            
            jsonArray.append(dict1 as NSDictionary)
            i = i + 1
        }
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonArray, options: .prettyPrinted)
            // here "jsonData" is the dictionary encoded in JSON data
            
            let theJSONText = NSString(data: jsonData,
                                       encoding: String.Encoding.ascii.rawValue)
            // here "decoded" is of type `Any`, decoded from JSON data
        
            let postString = "data=\(theJSONText!)"
            self.GetCartList("home/addcartdetails", params: postString, check: "update")
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
}

class ListTableViewCell : UITableViewCell
{
    // listcell
    @IBOutlet var dishImage: UIImageView!
    @IBOutlet var dishName: UILabel!
    @IBOutlet var minusButton: UIButton!
    @IBOutlet var plusButton: UIButton!
    @IBOutlet var quantityLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var removeButton: UIButton!
}

class Total1TableViewCell : UITableViewCell
{
    // totalcell
    @IBOutlet var totalQuantity: UILabel!
    @IBOutlet var totalPrice: UILabel!
}

extension CheckoutViewController : UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 1)
        {
            return productArray.count
        }
        else
        {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (indexPath.section == 0)
        {
          let cell = orderListTable.dequeueReusableCell(withIdentifier: "cell2")
        
          return cell!
        }
        else if (indexPath.section == 1)
        {
            let cell = orderListTable.dequeueReusableCell(withIdentifier: "listcell")! as! ListTableViewCell
            
            cell.minusButton.addTarget(self, action: #selector(minusValue(_:)), for: .touchUpInside)
            cell.plusButton.addTarget(self, action: #selector(plusValue(_:)), for: .touchUpInside)
            
            let dict = productArray[indexPath.row]
            cell.dishName.text = dict["product_name"] as? String
            let prc = dict["price"] as? String
            let qty = dict["quantity"] as? String
            let totalPrc = Float(prc!)! * Float(qty!)!
            let twoDecimalPlaces1 = String(format: "%.2f", totalPrc)
            cell.priceLabel.text = "\u{00A3}\(twoDecimalPlaces1)"
            cell.quantityLabel.text = dict["quantity"] as? String
            cell.dishImage.sd_setImage(with: NSURL(string : "https://www.sweettoothapp.co.uk/\(dict["image"] as! String)") as URL!)
            
            totalQuantity = totalQuantity + Int(cell.quantityLabel.text!)!
            
            cell.removeButton.addTarget(self, action: #selector(productRemoved(_:)), for: .touchUpInside)
            
            return cell
        }
        else
        {
            let cell = orderListTable.dequeueReusableCell(withIdentifier: "total1cell")! as! Total1TableViewCell
            
            cell.totalQuantity.text = "\(totalQuantity)"
            let twoDecimalPlaces2 = String(format: "%.2f", totalPrice)
            cell.totalPrice.text = "\u{00A3}\(twoDecimalPlaces2)"
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 55
    }
    
    func minusValue(_ sender: UIButton!)
    {
        let point : CGPoint = sender.convert(CGPoint.zero, to:orderListTable)
        let indexPath1 = orderListTable.indexPathForRow(at: point)
        let cell = orderListTable.cellForRow(at: indexPath1!) as! ListTableViewCell
        let dict = productArray[(indexPath1?.row)!]
        
        var qq : Int = Int(cell.quantityLabel.text!)!   
        
        if (qq > 1)
        {
            qq = qq - 1
            cell.quantityLabel.text = "\(qq)"
            totalQuantity = totalQuantity - 1
    
            quantArray[(indexPath1?.row)!] = Int(NSNumber.init(integerLiteral: qq))
            //print("----- qqqqqq arrrr = \(quantArray)")
            
            let indexPath2 = IndexPath.init(row: 0, section: 2)
            //print("indexxxx 2222= \(indexPath2)")
            let cell1 = orderListTable.cellForRow(at: indexPath2) as! Total1TableViewCell
            cell1.totalQuantity.text =  "\(totalQuantity)"
            
            let prc = Float((dict["price"] as? String)!) // actual price
            let prc1 = prc! * Float(qq)
            let twoDecimalPlaces3 = String(format: "%.2f", prc1)
            cell.priceLabel.text = "\u{00A3}\(twoDecimalPlaces3)"
            totalPrice = totalPrice - Float((dict["price"] as? String)!)!
            let twoDecimalPlaces4 = String(format: "%.2f", totalPrice)
            cell1.totalPrice.text = "\u{00A3}\(twoDecimalPlaces4)"
        }
    }
    
    func plusValue(_ sender: UIButton!)
    {
        let point : CGPoint = sender.convert(CGPoint.zero, to:orderListTable)
        let indexPath1 = orderListTable.indexPathForRow(at: point)
        let cell = orderListTable.cellForRow(at: indexPath1!) as! ListTableViewCell
        let dict = productArray[(indexPath1?.row)!]
        
        var qq : Int = Int(cell.quantityLabel.text!)!
        qq = qq + 1
        cell.quantityLabel.text = "\(qq)"
        totalQuantity = totalQuantity + 1
        
        quantArray[(indexPath1?.row)!] = Int(NSNumber.init(integerLiteral: qq))
        
        let indexPath2 = IndexPath.init(row: 0, section: 2)
        print("indexxxx 2222= \(indexPath2.section)   \(indexPath2.row)")
        let cell1 = orderListTable.cellForRow(at: indexPath2) as! Total1TableViewCell
        cell1.totalQuantity.text =  "\(totalQuantity)"
        
        let prc = Float((dict["price"] as? String)!) // actual price
        let prc1 = prc! * Float(qq)
        let twoDecimalPlaces5 = String(format: "%.2f", prc1)
        cell.priceLabel.text = "\u{00A3}\(twoDecimalPlaces5)"
        totalPrice = totalPrice + Float((dict["price"] as? String)!)!
        let twoDecimalPlaces6 = String(format: "%.2f", totalPrice)
        cell1.totalPrice.text = "\u{00A3}\(twoDecimalPlaces6)"
    }
    
    func productRemoved(_ sender : UIButton) {
        
        let point : CGPoint = sender.convert(CGPoint.zero, to: orderListTable)
        let indexPath1 = orderListTable.indexPathForRow(at: point)
        let dict = productArray[(indexPath1?.row)!]
        
        let parameters = "user_id=\(self.userid)&id=\((dict["id"] as? String)!)"
        print("pppppppp = \(parameters)")
        self.GetCartList("home/removecartdetails", params: parameters, check: "remove")
    }
}
