//
//  AppDelegate.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 3/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import Stripe


extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.barTintColor = UIColor.init(red: 250/255.0, green: 103/255.0, blue: 194/255.0, alpha: 1)

        UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(red: 250/255.0, green: 103/255.0, blue: 194/255.0, alpha: 1)
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
        
        let attrs = [
            NSForegroundColorAttributeName: UIColor.white,
            NSFontAttributeName: UIFont(name: "Open Sans", size: 18)!
        ]
        UINavigationBar.appearance().titleTextAttributes = attrs

        STPPaymentConfiguration.shared().publishableKey = " pk_test_FxoLlNszcS1ihYt8XdnxpVrc"
        
        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: "AWagrlbcDlBySBg8HRjKpW3zhjCkXXoV6CQa1ck4n5vCz7vMEQod6mQsFLNPpuzhsHx6NwGLuA21YL6t"])
        
        let user = UserDefaults.standard
        let session = user.object(forKey: "logout") as? Bool
        if (session == nil) || (session == false)
        {
        }
        else {
            self.add111()
        }
        return true
    }

    func add111()
    {
        self.window?.rootViewController = ContainerViewController.loadMainControllerWithHamburgerMenu()
        self.window?.makeKeyAndVisible()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

