//
//  DashboardSearchVC.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 3/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import SystemConfiguration

class DashboardSearchVC: UIViewController {

    @IBOutlet var cartItems: UILabel!
    
    @IBOutlet var searchTableView: UITableView!
    
    @IBOutlet var navBar: UINavigationBar!
   
    var pasArray : [NSDictionary] = []
    var pincode : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        pincode = pincode.uppercased()
        navBar.topItem?.title = pincode
        
        DispatchQueue.main.async(execute: {() -> Void in
            self.searchTableView.reloadData()
        })
    }
    
    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        cartItems.text = (user.object(forKey: "itemcount") as! String)
    }
    
    @IBAction func openCart(_ sender: Any) {
        if (self.cartItems.text == "") || (self.cartItems.text == "0") {
            ApiResponse.alert(title: "Oooops", message: "Cart is empty", controller: self)
        }
        else {
            let cart = self.storyboard?.instantiateViewController(withIdentifier: "cart") as! MyCartViewController
            self.present(cart, animated: true, completion: nil)
        }
    }
    
    @IBAction func onback(_ sender: Any) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.add111()
    }
}

class SearchTableViewCell : UITableViewCell {
    // searchcell
    
    @IBOutlet var shopImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var addLabel: UILabel!
    @IBOutlet var contactLabel: UILabel!
    @IBOutlet var deliveryFeeLabel: UILabel!
    @IBOutlet var minimumOrder: UILabel!
    @IBOutlet var deliveryArea: UILabel!
    @IBOutlet var closeTime: UILabel!
    @IBOutlet var openTime: UILabel!
    
    @IBOutlet var starCollection: [UIImageView]!
    @IBOutlet var addHtConstraint: NSLayoutConstraint!
}

extension DashboardSearchVC : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pasArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SearchTableViewCell = searchTableView.dequeueReusableCell(withIdentifier: "searchcell") as! SearchTableViewCell
        
        let dict = pasArray[indexPath.row]
        cell.shopImage.sd_setImage(with: NSURL(string : "\(Constant.BASE_URL)\(dict["image"] as! String)")! as URL)
        cell.nameLabel.text = dict["restaurant_name"] as? String
        cell.addLabel.text = dict["address"] as? String
        let ht = ApiResponse.calculateHeightForString((dict["address"] as? String)!)
        cell.addHtConstraint.constant = 20 + ht
        cell.addLabel.numberOfLines = 0
        
        cell.deliveryFeeLabel.text = "Delivery fee : \u{00A3}\(dict["delivery_fee"] as! String)"
        let cn = dict["contact_number"] as! String
        if (cn != "") {
            cell.contactLabel.text = "Contact no : \(cn)"
        }
        else {
            cell.contactLabel.text = "Contact no : NA"
        }
        cell.minimumOrder.text = "Minimum order : \u{00A3}\(dict["minimum_order"] as! String)"
        cell.deliveryArea.text = "Delivery area : \(dict["delevery_area_radius"] as! String) mi"
        cell.openTime.text = "    Opening time : \(dict["opening_time"] as! String)"
        cell.closeTime.text = "   Closing time : \(dict["closing_time"] as! String)"
        
        cell.openTime.layer.borderColor = UIColor.init(red: 250/255.0, green: 103/255.0, blue: 194/255.0, alpha: 1).cgColor
        cell.openTime.layer.borderWidth = 0.6
        cell.closeTime.layer.borderColor = UIColor.init(red: 250/255.0, green: 103/255.0, blue: 194/255.0, alpha: 1).cgColor
        cell.closeTime.layer.borderWidth = 0.6
        
        ApiResponse.callRating(Double(dict["rating"] as! String)!, starRating: cell.starCollection)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = pasArray[indexPath.row]
        
        let data = self.storyboard?.instantiateViewController(withIdentifier: "catdata") as! CategoryDataVC
        data.restaurentId = dict["id"] as! String
        data.restName = dict["restaurant_name"] as! String
        self.present(data, animated: true, completion: nil)
    }
}
