//
//  ViewController.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 3/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import ReachabilitySwift
import SystemConfiguration

var reach : Reachability = Reachability()!
var user : UserDefaults = UserDefaults.standard

class ViewController: UIViewController, UITextFieldDelegate, UIWebViewDelegate {

    @IBOutlet var usernameText: UITextField!
    @IBOutlet var passwordText: UITextField!
    
    @IBOutlet var signinbutton: UIButton!
    @IBOutlet var registerButton: UIButton!
    
    var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let textfield : [UITextField] = [usernameText, passwordText]
        let placeholder = ["Email Id", "Password"]
        var i = 0
        
        for tf in textfield
        {
            tf.delegate = self
            let paddingForFirst = UIView(frame:  CGRect(x: 0, y: 0, width: 70, height: tf.frame.size.height))
            //Adding the padding to the second textField
            tf.leftView = paddingForFirst
            tf.leftViewMode = UITextFieldViewMode .always
            
            tf.attributedPlaceholder = NSAttributedString(string:placeholder[i],
                                                             attributes:[NSForegroundColorAttributeName: UIColor.white])
            i += 1
        }
        
        signinbutton.layer.cornerRadius = 20
        registerButton.layer.cornerRadius = 20
        registerButton.layer.borderWidth = 1.0
        registerButton.layer.borderColor = UIColor.white.cgColor
    }
    
    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    @IBAction func onSignin(_ sender: Any) {
        
        if (usernameText.text == "") || (passwordText.text == "") {
            ApiResponse.alert(title: "Blank", message: "Fields cannot be blanks", controller: self)
        }
        else if (ApiResponse.validateEmail(usernameText.text!) == false) {
            ApiResponse.alert(title: "Invalid Email", message: "Please enter valid email id", controller: self)
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN {
           
            LoadingIndicatorView.show("Loading....")
            let params = "email=\(usernameText.text!)&password=\(passwordText.text!)"
            
            ApiResponse.onResponsePostPhp(url: "home/attemptlogin", parms: params, completion: { (result, error) in
                
                if (error == "") {
                    print("login result = \(result)")
                    
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    
                    if (status == true) {
                        
                        OperationQueue.main.addOperation {
                            user.set(true, forKey: "logout")
                            let datadict = result["data"] as! NSDictionary
                            user.set(datadict, forKey: "logindata")
                            user.synchronize()
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.add111()
                        }
                    }
                    else {
                        ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                        ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                        ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
             ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }

}

extension String {
    
    var first: String {
        return String(characters.prefix(1))
    }
    var last: String {
        return String(characters.suffix(1))
    }
    var uppercaseFirst: String {
        return first.uppercased() + String(characters.dropFirst())
    }
    
}


