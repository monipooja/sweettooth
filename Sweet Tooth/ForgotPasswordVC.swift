//
//  ForgotPasswordVC.swift
//  Sweet Tooth
//
//  Created by Prashant Shinde on 3/28/17.
//  Copyright © 2017 Prashant Shinde. All rights reserved.
//

import UIKit
import SystemConfiguration

class ForgotPasswordVC: UIViewController, UITextFieldDelegate {

    @IBOutlet var emailText: UITextField!
    @IBOutlet var confirmButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        emailText.delegate = self
        let paddingForFirst = UIView(frame:  CGRect(x: 0, y: 0, width: 70, height: emailText.frame.size.height))
        //Adding the padding to the second textField
        emailText.leftView = paddingForFirst
        emailText.leftViewMode = UITextFieldViewMode .always
        
        emailText.attributedPlaceholder = NSAttributedString(string:"Email Id",
                                                      attributes:[NSForegroundColorAttributeName: UIColor.white])
        
         confirmButton.layer.cornerRadius = 20
    }
    
    func ipv6Reachability() -> SCNetworkReachability?
    {
        var zeroAddress = sockaddr_in6()
        zeroAddress.sin6_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin6_family = sa_family_t(AF_INET6)
        
        return withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func onConfirm(_ sender: Any) {
        
        if (emailText.text == "") {
            ApiResponse.alert(title: "Email Id blank", message: "Email id cannot be blanks", controller: self)
        }
        else if (ApiResponse.validateEmail(emailText.text!) == false) {
            ApiResponse.alert(title: "Invalid Email", message: "Please enter valid email id", controller: self)
        }
        else if reach.isReachable || reach.isReachableViaWiFi || reach.isReachableViaWWAN {
            
            emailText.text = emailText.text?.lowercased()
            
            LoadingIndicatorView.show()
            let params = "email=\(emailText.text!)"
            ApiResponse.onResponsePostPhp(url: "home/forgotpassword", parms: params, completion: { (result, error) in
                
                if (error == "") {
                    
                    let status = result["status"] as! Bool
                    let message = result["message"] as! String
                    if (status == true) {
                        OperationQueue.main.addOperation {
                            self.emailText.text = ""
                            ApiResponse.alert(title: "Done", message: message, controller: self)
                        }
                    }
                    else {
                        ApiResponse.alert(title: "Ooooops", message: message, controller: self)
                    }
                }
                else {
                    if (error == Constant.Status_Not_200) {
                         ApiResponse.alert(title: "Something went wrong", message: "Please try again", controller: self)
                    }
                    else {
                       ApiResponse.alert(title: "Request time out", message: "Please check your internet", controller: self)
                    }
                }
            })
        }
        else {
           ApiResponse.alert(title: "No Internet", message: "No internet connection", controller: self)
        }
    }
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
